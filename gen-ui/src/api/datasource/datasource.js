import request from '@/utils/request'

// 查询数据源管理列表
export function pageDatasource(query) {
  return request({
    url: '/datasource/page',
    method: 'post',
    data: query
  })
}

// 查询数据源管理列表
export function listDatasource(query) {
  return request({
    url: '/datasource/list',
    method: 'post',
    data: query
  })
}

// 查询数据源管理详细
export function getDatasource(id) {
  return request({
    url: '/datasource/' + id,
    method: 'get'
  })
}

// 新增数据源管理
export function addDatasource(data) {
  return request({
    url: '/datasource',
    method: 'post',
    data: data
  })
}

// 修改数据源管理
export function updateDatasource(data) {
  return request({
    url: '/datasource',
    method: 'put',
    data: data
  })
}

// 删除数据源管理
export function delDatasource(id) {
  return request({
    url: '/datasource/' + id,
    method: 'delete'
  })
}

// 测试数据源
export function testDatasource(data) {
  return request({
    url: '/datasource/test',
    method: 'post',
    data: data
  })
}
