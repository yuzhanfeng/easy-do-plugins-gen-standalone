SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `data_source_manager`;
CREATE TABLE `data_source_manager` (
                                       `id` varchar(32) NOT NULL COMMENT '主键',
                                       `source_name` varchar(32) NULL DEFAULT NULL COMMENT '数据源名称',
                                       `source_code` varchar(12) NULL DEFAULT NULL COMMENT '数据源编码',
                                       `source_type` varchar(12) NULL DEFAULT NULL COMMENT '数据源类型',
                                       `url` varchar(255) NULL DEFAULT NULL COMMENT 'URL',
                                       `user_name` varchar(64) NULL DEFAULT NULL COMMENT '用户名',
                                       `password` varchar(64) NULL DEFAULT NULL COMMENT '密码',
                                       `params` json NULL COMMENT '参数',
                                       `state` tinyint(2) NOT NULL DEFAULT 1 COMMENT '状态(0停用 1启用)',
                                       `create_by` varchar(20) NULL DEFAULT '1' COMMENT '创建人',
                                       `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                       `update_by` varchar(20) NULL DEFAULT NULL COMMENT '修改人',
                                       `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
                                       `remark` varchar(255) NULL DEFAULT NULL COMMENT '备注',
                                       `del_flag` int(1) NULL DEFAULT 0
) ENGINE = InnoDB ROW_FORMAT = Dynamic;
INSERT INTO `data_source_manager`
VALUES ('03213555bfac52e8d1d017be09f263a9', '测试http数据源', NULL, 'HTTP', 'GET:http://172.30.1.180:30003/x-data-resource-service/v2/api-docs'
       , NULL, NULL, NULL, 1, '1'
       , '2021-12-30 16:23:13', NULL, '2022-01-17 17:04:51', NULL, 0);
INSERT INTO `data_source_manager`
VALUES ('0f246e6cc7ab9a5048ae5f7db820ea75', 'bilibli', 'bilibili', 'HTTP', 'GET:https://api.bilibili.com/x/v3/fav/folder/collected/list?pn=1&ps=20&up_mid=383440665&platform=web&jsonp=jsonp'
       , NULL, NULL, '{}', 1, '1'
       , '2021-12-30 22:01:42', NULL, NULL, NULL, 0);
INSERT INTO `data_source_manager`
VALUES ('2e8a49b0d1fa381e1bcb65a3d82f2624', '第二个', '123123', 'MYSQL', 'jdbc:mysql://123.57.174.32:3306/database-gen?useUnicode=true&characterEncoding=utf8'
       , 'database-gen', 'database-gen', NULL, 1, '1'
       , '2021-11-18 17:32:57', NULL, '2021-11-22 12:41:56', NULL, 1);
INSERT INTO `data_source_manager`
VALUES ('44e9f13092ff259099c7fb63fc6afb5a', '测试2', '123', 'MYSQL', 'jdbc:mysql://123.57.174.32:3306/easy-do-gen?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8'
       , 'easy-do-gen', 'easy-do-gen', NULL, 1, '1'
       , '2021-11-29 15:10:18', NULL, NULL, NULL, 1);
INSERT INTO `data_source_manager`
VALUES ('46bc18e2d65565d64d050fd85f23840b', 'easydo', NULL, 'MYSQL', 'jdbc:mysql://123.57.174.32:3306/easy-do?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8'
       , 'easy-do', 'easy-do', NULL, 1, '1'
       , '2021-12-27 15:22:53', NULL, NULL, NULL, 0);
INSERT INTO `data_source_manager`
VALUES ('67739c61685911508b1be7c78f0f5b47', '安顺-数据库', 'anshun', 'MYSQL', 'jdbc:mysql://172.30.1.250:3306/govaffair_anshun_exchange_platform?useSSL=false&useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true'
       , 'root', '1qazxcde32wsX', NULL, 1, '1'
       , '2021-11-22 09:52:09', NULL, NULL, NULL, 1);
INSERT INTO `data_source_manager`
VALUES ('b4e8b6395eea2f2973dd4a53e25b3118', 'bilibili', 'bilibili', 'HTTP', 'GET:https://api.bilibili.com/x/v3/fav/folder/collected/list?pn=1&ps=20&up_mid=383440665&platform=web&jsonp=jsonp'
       , NULL, NULL, '{}', 1, '1'
       , '2021-12-30 21:52:08', NULL, '2021-12-30 21:58:08', NULL, 1);
INSERT INTO `data_source_manager`
VALUES ('f77131183a15d0097625104e7df0daeb', '数据库文档', 'doc', 'MYSQL', 'jdbc:mysql://123.57.174.32:3306/database-gen?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8'
       , 'database-gen', 'database-gen', NULL, 1, '1'
       , '2021-11-30 10:23:04', NULL, '2021-12-15 12:56:47', NULL, 0);
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
                             `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                             `data_source_id` varchar(64) NOT NULL COMMENT '数据源',
                             `table_name` varchar(200) NULL DEFAULT '' COMMENT '表名称',
                             `table_comment` varchar(500) NULL DEFAULT '' COMMENT '表描述',
                             `sub_table_name` varchar(64) NULL DEFAULT NULL COMMENT '关联子表的表名',
                             `sub_table_fk_name` varchar(64) NULL DEFAULT NULL COMMENT '子表关联的外键名',
                             `class_name` varchar(100) NULL DEFAULT '' COMMENT '实体类名称',
                             `tpl_category` varchar(200) NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
                             `package_name` varchar(100) NULL DEFAULT NULL COMMENT '生成包路径',
                             `module_name` varchar(30) NULL DEFAULT NULL COMMENT '生成模块名',
                             `business_name` varchar(30) NULL DEFAULT NULL COMMENT '生成业务名',
                             `function_name` varchar(50) NULL DEFAULT NULL COMMENT '生成功能名',
                             `function_author` varchar(50) NULL DEFAULT NULL COMMENT '生成功能作者',
                             `gen_type` char(1) NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
                             `gen_path` varchar(200) NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
                             `options` varchar(1000) NULL DEFAULT NULL COMMENT '其它生成选项',
                             `create_by` varchar(64) NULL DEFAULT '' COMMENT '创建者',
                             `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                             `update_by` varchar(64) NULL DEFAULT '' COMMENT '更新者',
                             `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                             `remark` varchar(500) NULL DEFAULT NULL COMMENT '备注',
                             `is_query` int(1) NULL DEFAULT 1 COMMENT '是否生成查询',
                             `is_insert` int(1) NULL DEFAULT 1 COMMENT '是否生成插入',
                             `is_update` int(1) NULL DEFAULT 1 COMMENT '是否生成更新语句',
                             `is_remove` int(1) NULL DEFAULT 1 COMMENT '是否生成删除语句',
                             `is_manager` int(1) NULL DEFAULT 1 COMMENT '是否生成manager层',
                             `template_ids` varchar(255) NULL DEFAULT NULL COMMENT '模板',
                             `is_wrapper` int(1) NULL DEFAULT 0 COMMENT '是否使用lambda条件'
) ENGINE = InnoDB ROW_FORMAT = Dynamic;
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
                                    `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
                                    `table_id` varchar(64) NULL DEFAULT NULL COMMENT '归属表编号',
                                    `column_name` varchar(200) NULL DEFAULT NULL COMMENT '列名称',
                                    `column_comment` varchar(500) NULL DEFAULT NULL COMMENT '列描述',
                                    `column_type` varchar(100) NULL DEFAULT NULL COMMENT '列类型',
                                    `java_type` varchar(500) NULL DEFAULT NULL COMMENT 'JAVA类型',
                                    `java_field` varchar(200) NULL DEFAULT NULL COMMENT 'JAVA字段名',
                                    `is_pk` char(1) NULL DEFAULT NULL COMMENT '是否主键（1是）',
                                    `is_increment` char(1) NULL DEFAULT NULL COMMENT '是否自增（1是）',
                                    `is_required` char(1) NULL DEFAULT NULL COMMENT '是否必填（1是）',
                                    `is_insert` char(1) NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
                                    `is_edit` char(1) NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
                                    `is_list` char(1) NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
                                    `is_query` char(1) NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
                                    `query_type` varchar(200) NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
                                    `html_type` varchar(200) NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
                                    `dict_type` varchar(200) NULL DEFAULT '' COMMENT '字典类型',
                                    `sort` int(11) NULL DEFAULT NULL COMMENT '排序',
                                    `create_by` varchar(64) NULL DEFAULT '' COMMENT '创建者',
                                    `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                    `update_by` varchar(64) NULL DEFAULT '' COMMENT '更新者',
                                    `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                    `default_value` varchar(255) NULL DEFAULT NULL COMMENT '字段默认值'
) ENGINE = InnoDB ROW_FORMAT = Dynamic;
DROP TABLE IF EXISTS `gen_table_index`;
CREATE TABLE `gen_table_index` (
                                   `index_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                   `table_id` int(11) NULL DEFAULT NULL COMMENT '表信息主键',
                                   `TABLE_NAME` varchar(255) NULL DEFAULT NULL COMMENT '表名称',
                                   `NON_UNIQUE` varchar(255) NULL DEFAULT NULL COMMENT '该索引是否是唯一索引。若不是唯一索引，则该列的值为 1；若是唯一索引，则该列的值为 0',
                                   `INDEX_NAME` varchar(255) NULL DEFAULT NULL COMMENT '索引名称',
                                   `Seq_in_index` varchar(8) NULL DEFAULT NULL COMMENT '该列在索引中的位置',
                                   `COLUMN_NAME` varchar(255) NULL DEFAULT NULL COMMENT '索引的列字段',
                                   `COLLATION` varchar(8) NULL DEFAULT NULL COMMENT '列以何种顺序存储在索引中。在 MySQL 中，升序显示值“A”（升序），若显示为 NULL，则表示无分类。',
                                   `CARDINALITY` varchar(8) NULL DEFAULT NULL COMMENT '索引中唯一值数目的估计值',
                                   `SUB_PART` varchar(8) NULL DEFAULT NULL COMMENT '列中被编入索引的字符的数量。若列只是部分被编入索引，则该列的值为被编入索引的字符的数目；若整列被编入索引，则该列的值为 NULL',
                                   `PACKED` varchar(8) NULL DEFAULT NULL COMMENT '关键字如何被压缩。若没有被压缩，值为 NULL',
                                   `NULLABLE` varchar(8) NULL DEFAULT NULL COMMENT '索引列中是否包含 NULL。若列含有 NULL，该列的值为 YES。若没有，则该列的值为 NO',
                                   `INDEX_TYPE` varchar(16) NULL DEFAULT NULL COMMENT '索引使用的类型和方法（BTREE、FULLTEXT、HASH、RTREE）',
                                   `COMMENT` varchar(255) NULL DEFAULT NULL COMMENT '注释',
                                   `INDEX_COMMENT` varchar(255) NULL DEFAULT NULL COMMENT '索引注释'
) ENGINE = InnoDB ROW_FORMAT = Dynamic;
DROP TABLE IF EXISTS `template_management`;
CREATE TABLE `template_management` (
                                       `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
                                       `template_name` varchar(32) NULL DEFAULT NULL COMMENT '模板名称',
                                       `template_code` longtext NOT NULL COMMENT '代码',
                                       `package_path` varchar(255) NULL DEFAULT NULL COMMENT '包路径',
                                       `file_name` varchar(32) NULL DEFAULT NULL COMMENT '文件名',
                                       `file_path` varchar(255) NULL DEFAULT NULL COMMENT '文件路径',
                                       `create_by` varchar(16) NULL DEFAULT NULL COMMENT '创建者',
                                       `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
                                       `update_by` varchar(16) NULL DEFAULT NULL COMMENT '更新者',
                                       `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
                                       `remark` varchar(255) NULL DEFAULT NULL COMMENT '备注',
                                       `del_flag` int(1) NULL DEFAULT 0 COMMENT '删除标志'
) ENGINE = InnoDB ROW_FORMAT = Dynamic;
INSERT INTO `template_management`
VALUES (1, '系统默认-controller.java', 'package ${packageName}.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import plus.easydo.core.result.R;
import plus.easydo.core.vaild.Insert;
import plus.easydo.core.vaild.Update;
import ${packageName}.dto.${ClassName}Dto;
import ${packageName}.qo.${ClassName}Qo;
import ${packageName}.service.I${ClassName}Service;
import ${packageName}.vo.${ClassName}Vo;
import plus.easydo.starter.mybatis.plus.base.MPBaseController;
import plus.easydo.starter.mybatis.plus.result.MPDataResult;
import plus.easydo.starter.oauth.resources.anotation.CustomizePreAuthorize;

import java.util.List;

/**
 * ${functionName}Controller
 *
 * @author ${author}
 * @date ${datetime}
 */
@Api(tags = "${functionName}")
@RestController
@RequestMapping("/${businessName}")
public class ${ClassName}Controller extends MPBaseController {

    @Autowired
    private I${ClassName}Service ${className}Service;

#if(${isQuery} == 1)
    /**
     * 分页条件查询${functionName}列表
     */
    @ApiOperation(value = "分页条件查询${functionName}列表")
    @CustomizePreAuthorize(hasPermission = {"${permissionPrefix}:list"})
    @PostMapping("/page")
    public MPDataResult<${ClassName}Vo> page(@RequestBody ${ClassName}Qo qo) {
        IPage<${ClassName}Vo> page = ${className}Service.page(qo);
        return ok(page);
    }

    /**
     * 获取所有${functionName}列表
     */
    @ApiOperation(value = "获取所有${functionName}列表")
    @CustomizePreAuthorize(hasPermission = {"${permissionPrefix}:list"})
    @GetMapping("/list")
    public R<List<${ClassName}Vo>> list(@RequestBody ${ClassName}Qo qo) {
        return ok(${className}Service.list(qo));
    }


    /**
     * 获取${functionName}详细信息
     */
    @ApiOperation(value = "获取${functionName}详细信息")
    @CustomizePreAuthorize(hasPermission = {"${permissionPrefix}:query"})
    @GetMapping(value = "/{${pkColumn.javaField}}")
    public R<Object> getInfo(@PathVariable("${pkColumn.javaField}") ${pkColumn.javaType} ${pkColumn.javaField}) {
        return ok(${className}Service.selectById(${pkColumn.javaField}));
    }
#end

#if(${isInsert} == 1)
    /**
     * 新增${functionName}
     */
    @ApiOperation(value = "新增${functionName}")
    @CustomizePreAuthorize(hasPermission = {"${permissionPrefix}:add"})
    @PostMapping
    public R<Object> add(@RequestBody @Validated(Insert.class) ${ClassName}Dto dto) {
        return opResult(${className}Service.insert(dto));
    }
#end

#if(${isUpdate} == 1)
    /**
     * 修改${functionName}
     */
    @ApiOperation(value = "修改${functionName}")
    @CustomizePreAuthorize(hasPermission = {"${permissionPrefix}:edit"})
    @PutMapping
    public R<Object> edit(@RequestBody @Validated(Update.class) ${ClassName}Dto dto) {
        return opResult(${className}Service.update(dto));
    }
#end

#if(${isRemove} == 1)
    /**
     * 删除${functionName}
     */
    @ApiOperation(value = "删除${functionName}")
    @CustomizePreAuthorize(hasPermission = {"${permissionPrefix}:remove"})
	@DeleteMapping("/{${pkColumn.javaField}s}")
    public R<Object> remove(@PathVariable ${pkColumn.javaType}[] ${pkColumn.javaField}s) {
        return opResult(${className}Service.deleteByIds(${pkColumn.javaField}s));
    }
#end
}
', NULL, 'controller.java.vm'
       , 'main/java/#{packageName}/controller/#{className}Controller.java', NULL, '2021-11-22 09:38:31', NULL, '2021-11-23 10:24:16'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (2, '系统默认-dto.java', 'package ${packageName}.dto;

#foreach ($import in $importList)
import ${import};
#end
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import plus.easydo.core.vaild.Insert;
import plus.easydo.core.vaild.Update;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
/**
 * ${functionName}数据传输对象
 *
 * @author ${author}
 * @date ${datetime}
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class ${ClassName}Dto implements Serializable {
    private static final long serialVersionUID = 1L;

#foreach ($column in $columns)
#if(!$table.isSuperColumn($column.javaField))
    /** $column.columnComment */
#if($column.list)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
    @ApiModelProperty(value = "${comment}")
###if($parentheseIndex != -1)
##    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
###elseif($column.javaType == ''DaLocalDateTimete'')
##    @JsonFormat(pattern = "yyyy-MM-dd")
##    @Excel(name = "${comment}", width = 30, dateFormat = "yyyy-MM-dd")
###else
##    @Excel(name = "${comment}")
###end
#if($column.isRequired)
#if($column.isInsert)
#if($column.isEdit)
    @NotNull(message = "${comment}必填",groups = {Insert.class, Update.class})
#else
    @NotNull(message = "${comment}必填",groups = {Insert.class})
#end
#elseif($column.isEdit)
    @NotNull(message = "${comment}必填",groups = {Update.class})
#end
#end
#end
    private $column.javaType $column.javaField;

#end
#end
#if($table.sub)
    /** $table.subTable.functionName信息 */
    private List<${subClassName}> ${subclassName}List;

#end
}
', NULL, 'dto.java.vm'
       , 'main/java/#{packageName}/dto/#{className}Dto.java', NULL, '2021-11-22 09:39:02', NULL, '2022-01-17 17:05:11'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (3, '系统默认-entity.java', 'package ${packageName}.entity;

#foreach ($import in $importList)
import ${import};
#end
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
#if($table.crud || $table.sub)
import plus.easydo.starter.mybatis.plus.entity.MPBaseEntity;
#elseif($table.tree)
import com.ruoyi.common.core.web.domain.TreeEntity;
#end

/**
 * ${functionName}数据库映射对象
 *
 * @author ${author}
 * @date ${datetime}
 */
#if($table.crud || $table.sub)
#set($Entity="MPBaseEntity")
#elseif($table.tree)
#set($Entity="TreeEntity")
#end
@Data
@ToString
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("${tableName}")
public class ${ClassName} extends ${Entity}
{
    private static final long serialVersionUID = 1L;

#foreach ($column in $columns)
#if(!$table.isSuperColumn($column.javaField))
    /** $column.columnComment */
#if($column.list)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($column.isPk == 1)
    @TableId(value = "${column.columnName}" , type = IdType.AUTO)
#else
    @TableField(value = "${column.columnName}")
#end
    @ApiModelProperty(value = "${comment}")
#if($parentheseIndex != -1)
#elseif($column.javaType == ''LocalDateTime'')
    @JsonFormat(pattern = "yyyy-MM-dd")
#else
#end
#end
    private $column.javaType $column.javaField;

#end
#end
}
', NULL, 'entity.java.vm'
       , 'main/java/#{packageName}/entity/#{className}Entity.java', NULL, '2021-11-22 09:39:52', NULL, '2021-11-22 12:52:51'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (4, '系统默认-manager.java', 'package ${packageName}.manager;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import ${packageName}.dto.${ClassName}Dto;
import ${packageName}.entity.${ClassName};
import ${packageName}.qo.${ClassName}Qo;
import ${packageName}.vo.${ClassName}Vo;
import plus.easydo.starter.oauth.core.utils.Oauth2Utils;
import ${packageName}.mapper.${ClassName}Mapper;
import org.springframework.stereotype.Service;
import plus.easydo.utils.ResultConvertUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


/**
 * ${functionName}Manager层
 *
 * @author ${author}
 * @date ${datetime}
 */
@Service
public class ${ClassName}Manager extends ServiceImpl<${ClassName}Mapper, ${ClassName}> {

#if(${isQuery} == 1)
    /**
     * 查询${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}
     */
    @Override
    public ${ClassName}Vo selectById(${pkColumn.javaType} ${pkColumn.javaField}) {
        ${ClassName} ${className} = baseMapper.selectById(${pkColumn.javaField});
        return ResultConvertUtil.convertVo(${className},${ClassName}Vo::new);
    }
#end

#if(${isQuery} == 1)
    /**
     * 分页条件查询${functionName}列表
     *
     * @param qo 查询条件封装
     * @return ${functionName}
     */
    @Override
    public IPage<${ClassName}Vo> page(${ClassName}Qo qo) {
        IPage<${ClassName}> page = qo.startPage();
        IPage<${ClassName}> iPage = baseMapper.select${ClassName}List(page, qo);
        List<${ClassName}Vo> voList = ResultConvertUtil.copyList(iPage.getRecords(), ${ClassName}Vo::new);
        IPage<${ClassName}Vo> voPage = new Page<>();
        BeanUtils.copyProperties(iPage,voPage);
        voPage.setRecords(voList);
        return voPage;
    }
#end

#if(${isQuery} == 1)
    /**
     * 获取所有${functionName}列表
     *
     * @param qo 查询条件封装
     * @return ${functionName}
     */
    @Override
    public List<${ClassName}Vo> list(${ClassName}Qo qo) {
        List<${ClassName}> list = baseMapper.selectList(Wrappers.query());
        return ResultConvertUtil.copyList(list,${ClassName}Vo::new);
    }
#end

#if(${isInsert} == 1)
    /**
     * 新增${functionName}
     *
     * @param dto 数据传输对象
     * @return 结果
     */
    @Override
    public int insert(${ClassName}Dto dto) {
        ${ClassName} ${className} = ${ClassName}.builder().build();
        BeanUtils.copyProperties(dto, ${className});
        ${className}.buildCreateTime();
        ${className}.setCreateBy(Oauth2Utils.getUserIdString());
        return baseMapper.insert(${className});
    }
#end

#if(${isupdate} == 1)
    /**
     * 修改${functionName}
     *
     * @param dto 数据传输对象
     * @return 结果
     */
    @Override
    public int update(${ClassName}Dto dto) {
        ${ClassName} ${className} = ${ClassName}.builder().build();
        BeanUtils.copyProperties(dto, ${className});
        ${className}.buildUpdateTime();
        ${className}.setUpdateBy(Oauth2Utils.getUserIdString());
        return baseMapper.updateById(${className});
    }
#end

#if(${isRemove} == 1)
    /**
     * 批量删除${functionName}
     *
     * @param ${pkColumn.javaField}s 需要删除的${functionName}ID
     * @return 结果
     */
    @Override
    public Boolean deleteByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s) {
        return removeByIds(Arrays.asList(${pkColumn.javaField}s));
    }
#end

#if(${isRemove} == 1)
    /**
     * 删除${functionName}信息
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
    @Override
    public Boolean deleteById(${pkColumn.javaType} ${pkColumn.javaField}) {
        return removeById(${pkColumn.javaField});
    }
#end
}
', NULL, 'manager.java.vm'
       , 'main/java/#{packageName}/manager/#{className}Manager.java', NULL, '2021-11-22 09:41:04', NULL, '2021-11-22 12:49:01'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (5, '系统默认-mapper.java', 'package ${packageName}.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ${packageName}.entity.${ClassName};
import ${packageName}.qo.${ClassName}Qo;
import ${packageName}.entity.${ClassName};

/**
 * ${functionName}Mapper接口
 *
 * @author ${author}
 * @date ${datetime}
 */
@Mapper
public interface ${ClassName}Mapper extends BaseMapper<${ClassName}> {
    /**
     * 查询${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}
     */
    ${ClassName} select${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField});

    /**
     * 分页条件查询${functionName}列表
     *
     * @param page 分页数据
     * @param qo 查询条件封装
     * @return ${functionName}集合
     */
    IPage<${ClassName}> select${ClassName}List(IPage<${ClassName}> page, @Param("qo") ${ClassName}Qo qo);

    /**
     * 新增${functionName}
     *
     * @param ${className} ${functionName}
     * @return 结果
     */
    int insert${ClassName}(${ClassName} ${className});

    /**
     * 修改${functionName}
     *
     * @param ${className} ${functionName}
     * @return 结果
     */
    int update${ClassName}(${ClassName} ${className});

    /**
     * 删除${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
    int delete${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField});

    /**
     * 批量删除${functionName}
     *
     * @param ${pkColumn.javaField}s 需要删除的数据ID
     * @return 结果
     */
    int delete${ClassName}ByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s);
}
', NULL, 'mapper.java.vm'
       , 'main/java/#{packageName}/mapper/#{className}Mapper.java', NULL, '2021-11-22 09:41:33', NULL, '2021-11-22 12:49:10'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (6, '系统默认-qo.java', 'package ${packageName}.qo;

#foreach ($import in $importList)
import ${import};
#end
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import plus.easydo.starter.mybatis.plus.qo.MpBaseQo;

/**
 * ${functionName}查询对象
 *
 * @author ${author}
 * @date ${datetime}
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ${ClassName}Qo extends MpBaseQo {

#foreach ($column in $columns)
#if(!$table.isSuperColumn($column.javaField))
    /** $column.columnComment */
#if($column.list)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($parentheseIndex != -1)
    @ApiModelProperty(value = "${comment}")
#elseif($column.javaType == ''LocalDateTime'')
    @ApiModelProperty(value = "${comment}")
    @JsonFormat(pattern = "yyyy-MM-dd")
#else
    @ApiModelProperty(value = "${comment}")
#end
#end
    private $column.javaType $column.javaField;

#end
#end
#if($table.sub)
    /** $table.subTable.functionName信息 */
    private List<${subClassName}> ${subclassName}List;

#end
}
', NULL, 'qo.java.vm'
       , 'main/java/#{packageName}/qo/#{className}Qo.java', NULL, '2021-11-22 09:42:01', NULL, '2021-11-22 12:49:22'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (7, '系统默认-service.java', 'package ${packageName}.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${packageName}.dto.${ClassName}Dto;
import ${packageName}.qo.${ClassName}Qo;
import ${packageName}.vo.${ClassName}Vo;

import java.util.List;

/**
 * ${functionName}Service接口
 *
 * @author ${author}
 * @date ${datetime}
 */
public interface I${ClassName}Service {

#if(${isQuery} == 1)
    /**
     * 查询${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}
     */
    ${ClassName}Vo selectById(${pkColumn.javaType} ${pkColumn.javaField});

    /**
     * 分页条件查询${functionName}列表
     *
     * @param qo 条件封装
     * @return ${functionName}集合
     */
    IPage<${ClassName}Vo> page(${ClassName}Qo qo);

    /**
     * 获取所有${functionName}列表
     *
     * @param qo 条件封装
     * @return ${functionName}集合
     */
    List<${ClassName}Vo> list(${ClassName}Qo qo);
#end

#if(${isInsert} == 1)
    /**
     * 新增${functionName}
     *
     * @param dto 数据传输对象
     * @return 结果
     */
    int insert(${ClassName}Dto dto);
#end

#if(${isUpdate} == 1)
    /**
     * 修改${functionName}
     *
     * @param dto 数据传输对象
     * @return 结果
     */
    int update(${ClassName}Dto dto);
#end

#if(${isRemove} == 1)
    /**
     * 批量删除${functionName}
     *
     * @param ${pkColumn.javaField}s 需要删除的${functionName}ID
     * @return 结果
     */
    Boolean deleteByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s);

    /**
     * 删除${functionName}信息
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
    Boolean deleteById(${pkColumn.javaType} ${pkColumn.javaField});
#end
}
', NULL, 'service.java.vm'
       , 'main/java/#{packageName}/service/#{className}Service.java', NULL, '2021-11-22 09:42:28', NULL, '2021-11-22 12:49:27'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (8, '系统默认-serviceImpl.java', 'package ${packageName}.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import ${packageName}.dto.${ClassName}Dto;
import ${packageName}.entity.${ClassName};
import ${packageName}.qo.${ClassName}Qo;
import ${packageName}.vo.${ClassName}Vo;
#if(${isManager} == 1)
import org.springframework.beans.factory.annotation.Autowired;
import ${packageName}.manager.${ClassName}Manager;
#end
#if(${isWrapper} == 1)
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
#end
##import plus.easydo.starter.oauth.core.utils.Oauth2Utils;
import ${packageName}.mapper.${ClassName}Mapper;
import ${packageName}.service.I${ClassName}Service;
import org.springframework.stereotype.Service;
import plus.easydo.utils.ResultConvertUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


/**
 * ${functionName}Service层
 *
 * @author ${author}
 * @date ${datetime}
 */
@Service
public class ${ClassName}ServiceImpl
#if(${isManager} == 0)
extends ServiceImpl<${ClassName}Mapper, ${ClassName}> implements I${ClassName}Service
#end
{

#if(${isManager} == 1)
        @Autowired
        ${ClassName}Manager ${className}Manager;
#end


#if(${isQuery} == 1)
    /**
     * 查询${functionName}
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}
     */
    @Override
    public ${ClassName}Vo selectById(${pkColumn.javaType} ${pkColumn.javaField}) {
#if(${isManager} == 0)
        ${ClassName} ${className} = baseMapper.selectById(${pkColumn.javaField});
        return ResultConvertUtil.convertVo(${className},${ClassName}Vo::new);
#else
        return  ${className}Manager.selectById(${pkColumn.javaField});
#end
    }

    /**
     * 分页条件查询${functionName}列表
     *
     * @param qo 查询条件封装
     * @return ${functionName}
     */
    @Override
    public IPage<${ClassName}Vo> page(${ClassName}Qo qo) {
#if(${isManager} == 0)
        IPage<${ClassName}> page = qo.startPage();
        IPage<${ClassName}> iPage = baseMapper.select${ClassName}List(page, qo);
        List<${ClassName}Vo> voList = ResultConvertUtil.copyList(iPage.getRecords(), ${ClassName}Vo::new);
        IPage<${ClassName}Vo> voPage = new Page<>();
        BeanUtils.copyProperties(iPage,voPage);
        voPage.setRecords(voList);
        return voPage;
#else
        return ${className}Manager.page(qo);
#end
    }

    /**
     * 获取所有${functionName}列表
     *
     * @param qo 查询条件封装
     * @return ${functionName}
     */
    @Override
    public List<${ClassName}Vo> list(${ClassName}Qo qo) {
#if(${isManager} == 0)
        List<${ClassName}> list = baseMapper.selectList(Wrappers.query());
        return ResultConvertUtil.copyList(list,${ClassName}Vo::new);
#else
        return ${className}Manager.list(qo);
#end

    }
#end

#if(${isInsert} == 1)
    /**
     * 新增${functionName}
     *
     * @param dto 数据传输对象
     * @return 结果
     */
    @Override
    public int insert(${ClassName}Dto dto) {
#if(${isManager} == 0)
        ${ClassName} ${className} = ${ClassName}.builder().build();
        BeanUtils.copyProperties(dto, ${className});
        ${className}.buildCreateTime();
        ##      ${className}.setCreateBy(Oauth2Utils.getUserIdString());
        return baseMapper.insert(${className});
#else
        return ${className}Manager.insert(dto});
#end
    }
#end

#if(${isUpdate} == 1)
    /**
     * 修改${functionName}
     *
     * @param dto 数据传输对象
     * @return 结果
     */
    @Override
    public int update(${ClassName}Dto dto) {
#if(${isManager} == 0)
        ${ClassName} ${className} = ${ClassName}.builder().build();
        BeanUtils.copyProperties(dto, ${className});
        ${className}.buildUpdateTime();
        ${className}.setUpdateBy(Oauth2Utils.getUserIdString());
        return baseMapper.updateById(${className});
#else
        return ${className}Manager.update(dto});
#end
    }
#end

#if(${isRemove} == 1)
    /**
     * 批量删除${functionName}
     *
     * @param ${pkColumn.javaField}s 需要删除的${functionName}ID
     * @return 结果
     */
    @Override
    public Boolean deleteByIds(${pkColumn.javaType}[] ${pkColumn.javaField}s) {
#if(${isManager} == 0)
        return removeByIds(Arrays.asList(${pkColumn.javaField}s));
#else
        return ${className}Manager.removeByIds(Arrays.asList(${pkColumn.javaField}s));
#end
    }
#end

#if(${isRemove} == 1)
    /**
     * 删除${functionName}信息
     *
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
    @Override
    public Boolean deleteById(${pkColumn.javaType} ${pkColumn.javaField}) {
#if(${isManager} == 0)
        return removeById(${pkColumn.javaField});
#else
        return ${className}Manager.removeById(${pkColumn.javaField});
#end
    }
#end

}
', NULL, 'serviceImpl.java.vm'
       , 'main/java/#{packageName}/service/impl/#{className}ServiceImpl.java', NULL, '2021-11-22 09:43:06', NULL, '2021-11-22 12:49:39'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (9, '系统默认-vo.java', 'package ${packageName}.vo;

#foreach ($import in $importList)
import ${import};
#end
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import java.io.Serializable;

/**
 * ${functionName}数据展示对象
 *
 * @author ${author}
 * @date ${datetime}
 */
#if($table.crud || $table.sub)
#set($Entity="MPBaseEntity")
#elseif($table.tree)
#set($Entity="TreeEntity")
#end
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class ${ClassName}Vo implements Serializable {
    private static final long serialVersionUID = 1L;

#foreach ($column in $columns)
#if(!$table.isSuperColumn($column.javaField))
    /** $column.columnComment */
#if($column.list)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($parentheseIndex != -1)
    @ApiModelProperty(value = "${comment}")
#elseif($column.javaType == ''Date'')
    @ApiModelProperty(value = "${comment}")
    @JsonFormat(pattern = "yyyy-MM-dd")
#else
    @ApiModelProperty(value = "${comment}")
#end
#end
    private $column.javaType $column.javaField;

#end
#end
#if($table.sub)
    /** $table.subTable.functionName信息 */
    private List<${subClassName}> ${subclassName}List;

#end
}
', NULL, 'vo.java.vm'
       , 'main/java/#{packageName}/vo/#{className}Vo.java', NULL, '2021-11-22 09:43:35', NULL, '2021-11-22 12:49:45'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (10, '系统默认-api.js', 'import request from ''@/utils/request''

// 查询${functionName}列表
export function list${BusinessName}(query) {
  return request({
    url: ''/${moduleName}/${businessName}/page'',
    method: ''post'',
    data: query
  })
}

// 查询${functionName}详细
export function get${BusinessName}(${pkColumn.javaField}) {
  return request({
    url: ''/${moduleName}/${businessName}/'' + ${pkColumn.javaField},
    method: ''get''
  })
}

// 新增${functionName}
export function add${BusinessName}(data) {
  return request({
    url: ''/${moduleName}/${businessName}'',
    method: ''post'',
    data: data
  })
}

// 修改${functionName}
export function update${BusinessName}(data) {
  return request({
    url: ''/${moduleName}/${businessName}'',
    method: ''put'',
    data: data
  })
}

// 删除${functionName}
export function del${BusinessName}(${pkColumn.javaField}) {
  return request({
    url: ''/${moduleName}/${businessName}/'' + ${pkColumn.javaField},
    method: ''delete''
  })
}
', NULL, 'api.js.vm'
       , 'vue/api/#{moduleName}/#{businessName}.js', NULL, '2021-11-22 09:44:12', NULL, '2021-11-22 10:32:14'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (11, '系统默认-sql', '-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(''${functionName}'', ''${parentMenuId}'', ''1'', ''${businessName}'', ''${moduleName}/${businessName}/index'', 1, 0, ''C'', ''0'', ''0'', ''${permissionPrefix}:list'', ''#'', ''admin'', sysdate(), '''', null, ''${functionName}菜单'');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(''${functionName}查询'', @parentId, ''1'',  ''#'', '''', 1, 0, ''F'', ''0'', ''0'', ''${permissionPrefix}:query'',        ''#'', ''admin'', sysdate(), '''', null, '''');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(''${functionName}新增'', @parentId, ''2'',  ''#'', '''', 1, 0, ''F'', ''0'', ''0'', ''${permissionPrefix}:add'',          ''#'', ''admin'', sysdate(), '''', null, '''');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(''${functionName}修改'', @parentId, ''3'',  ''#'', '''', 1, 0, ''F'', ''0'', ''0'', ''${permissionPrefix}:edit'',         ''#'', ''admin'', sysdate(), '''', null, '''');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(''${functionName}删除'', @parentId, ''4'',  ''#'', '''', 1, 0, ''F'', ''0'', ''0'', ''${permissionPrefix}:remove'',       ''#'', ''admin'', sysdate(), '''', null, '''');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values(''${functionName}导出'', @parentId, ''5'',  ''#'', '''', 1, 0, ''F'', ''0'', ''0'', ''${permissionPrefix}:export'',       ''#'', ''admin'', sysdate(), '''', null, '''');', NULL, 'sql.vm'
       , '#{businessName}Menu.sql', NULL, '2021-11-22 09:44:42', NULL, '2021-11-22 10:32:42'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (12, '系统默认-mapper.xml', '<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${packageName}.mapper.${ClassName}Mapper">

    <resultMap type="${ClassName}" id="${ClassName}Result">
#foreach ($column in $columns)
        <result property="${column.javaField}"    column="${column.columnName}"    />
#end
    </resultMap>
#if($table.sub)

    <resultMap id="${ClassName}${subClassName}Result" type="${ClassName}" extends="${ClassName}Result">
        <collection property="${subclassName}List" notNullColumn="${subTable.pkColumn.columnName}" javaType="java.util.List" resultMap="${subClassName}Result" />
    </resultMap>

    <resultMap type="${subClassName}" id="${subClassName}Result">
#foreach ($column in $subTable.columns)
        <result property="${column.javaField}"    column="${column.columnName}"    />
#end
    </resultMap>
#end

#if(${isQuery} == 1)
    <sql id="select${ClassName}Vo">
        select#foreach($column in $columns) $column.columnName#if($velocityCount != $columns.size()),#end#end from ${tableName}
    </sql>

    <select id="select${ClassName}List" parameterType="${packageName}.qo.${ClassName}Qo" resultMap="${ClassName}Result">
        <include refid="select${ClassName}Vo"/>
        <where>
#foreach($column in $columns)
#set($queryType=$column.queryType)
#set($javaField=$column.javaField)
#set($javaType=$column.javaType)
#set($columnName=$column.columnName)
#set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
#if($column.query)
#if($column.queryType == "EQ")
            <if test="qo.$javaField != null #if($javaType == ''String'' ) and qo.$javaField.trim() != ''''#end"> and $columnName = #{qo.$javaField}</if>
#elseif($queryType == "NE")
            <if test="qo.$javaField != null #if($javaType == ''String'' ) and qo.$javaField.trim() != ''''#end"> and $columnName != #{qo.$javaField}</if>
#elseif($queryType == "GT")
            <if test="qo.$javaField != null #if($javaType == ''String'' ) and qo.$javaField.trim() != ''''#end"> and $columnName &gt; #{qo.$javaField}</if>
#elseif($queryType == "GTE")
            <if test="qo.$javaField != null #if($javaType == ''String'' ) and qo.$javaField.trim() != ''''#end"> and $columnName &gt;= #{qo.$javaField}</if>
#elseif($queryType == "LT")
            <if test="qo.$javaField != null #if($javaType == ''String'' ) and qo.$javaField.trim() != ''''#end"> and $columnName &lt; #{qo.$javaField}</if>
#elseif($queryType == "LTE")
            <if test="qo.$javaField != null #if($javaType == ''String'' ) and qo.$javaField.trim() != ''''#end"> and $columnName &lt;= #{qo.$javaField}</if>
#elseif($queryType == "LIKE")
            <if test="qo.$javaField != null #if($javaType == ''String'' ) and qo.$javaField.trim() != ''''#end"> and $columnName like concat(''%'', #{qo.$javaField}, ''%'')</if>
#elseif($queryType == "BETWEEN")
            <if test="qo.params != null">
            <if test="qo.params.begin$AttrName != null and qo.params.begin$AttrName != '''' and qo.params.end$AttrName != null and qo.params.end$AttrName != ''''"> and $columnName between #{qo.params.begin$AttrName} and #{qo.params.end$AttrName}</if>
            </if>
#end
#end
#end
            and del_flag = 0
        </where>
    </select>

    <select id="select${ClassName}ById" parameterType="${pkColumn.javaType}" resultMap="#if($table.sub)${ClassName}${subClassName}Result#else${ClassName}Result#end">
#if($table.crud || $table.tree)
        <include refid="select${ClassName}Vo"/>
        where ${pkColumn.columnName} = #{${pkColumn.javaField}}
#elseif($table.sub)
        select#foreach($column in $columns) a.$column.columnName#if($velocityCount != $columns.size()),#end#end,
           #foreach($column in $subTable.columns) b.$column.columnName#if($velocityCount != $subTable.columns.size()),#end#end

        from ${tableName} a
        left join ${subTableName} b on b.${subTableFkName} = a.${pkColumn.columnName}
        where a.${pkColumn.columnName} = #{${pkColumn.javaField}}
#end
    </select>
#end

#if(${isInsert} == 1)
    <insert id="insert${ClassName}" parameterType="${ClassName}"#if($pkColumn.increment) useGeneratedKeys="true" keyProperty="$pkColumn.javaField"#end>
        insert into ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
#foreach($column in $columns)
#if($column.columnName != $pkColumn.columnName || !$pkColumn.increment)
            <if test="$column.javaField != null#if($column.javaType == ''String'' && $column.required) and $column.javaField != ''''#end">$column.columnName,</if>
#end
#end
         </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
#foreach($column in $columns)
#if($column.columnName != $pkColumn.columnName || !$pkColumn.increment)
            <if test="$column.javaField != null#if($column.javaType == ''String'' && $column.required) and $column.javaField != ''''#end">#{$column.javaField},</if>
#end
#end
         </trim>
    </insert>

    <insert id="batch${subClassName}">
        insert into ${subTableName}(#foreach($column in $subTable.columns) $column.columnName#if($velocityCount != $subTable.columns.size()),#end#end) values
        <foreach item="item" index="index" collection="list" separator=",">
            (#foreach($column in $subTable.columns) #{item.$column.javaField}#if($velocityCount != $subTable.columns.size()),#end#end)
        </foreach>
    </insert>
#end

#if(${isUpdate} == 1)
    <update id="update${ClassName}" parameterType="${ClassName}">
        update ${tableName}
        <trim prefix="SET" suffixOverrides=",">
#foreach($column in $columns)
#if($column.columnName != $pkColumn.columnName)
            <if test="$column.javaField != null#if($column.javaType == ''String'' && $column.required) and $column.javaField != ''''#end">$column.columnName = #{$column.javaField},</if>
#end
#end
        </trim>
        where ${pkColumn.columnName} = #{${pkColumn.javaField}}
    </update>
#end

#if(${isRemove} == 1)
    <delete id="delete${ClassName}ById" parameterType="${pkColumn.javaType}">
        delete from ${tableName} where ${pkColumn.columnName} = #{${pkColumn.javaField}}
    </delete>

    <delete id="delete${ClassName}ByIds" parameterType="String">
        delete from ${tableName} where ${pkColumn.columnName} in
        <foreach item="${pkColumn.javaField}" collection="array" open="(" separator="," close=")">
            #{${pkColumn.javaField}}
        </foreach>
    </delete>
#if($table.sub)

    <delete id="delete${subClassName}By${subTableFkClassName}s" parameterType="String">
        delete from ${subTableName} where ${subTableFkName} in
        <foreach item="${subTableFkclassName}" collection="array" open="(" separator="," close=")">
            #{${subTableFkclassName}}
        </foreach>
    </delete>

    <delete id="delete${subClassName}By${subTableFkClassName}" parameterType="Long">
        delete from ${subTableName} where ${subTableFkName} = #{${subTableFkclassName}}
    </delete>
#end

#end
</mapper>', NULL, 'mapper.xml.vm'
       , 'main/resources/mapper/#{moduleName}/#{className}Mapper.xml', NULL, '2021-11-22 09:45:23', NULL, '2022-01-17 11:31:16'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (13, '系统默认-index.vue', '<template>
  <div class="app-container">
    <el-form :model="queryParams" ref="queryForm" :inline="true" v-show="showSearch" label-width="68px">
#foreach($column in $columns)
#if($column.query)
#set($dictType=$column.dictType)
#set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($column.htmlType == "input")
      <el-form-item label="${comment}" prop="${column.javaField}">
        <el-input
          v-model="queryParams.${column.javaField}"
          placeholder="请输入${comment}"
          clearable
          size="small"
          @keyup.enter.native="handleQuery"
        />
      </el-form-item>
#elseif(($column.htmlType == "select" || $column.htmlType == "radio") && "" != $dictType)
      <el-form-item label="${comment}" prop="${column.javaField}">
        <el-select v-model="queryParams.${column.javaField}" placeholder="请选择${comment}" clearable size="small">
          <el-option
            v-for="dict in ${column.javaField}Options"
            :key="dict.dictValue"
            :label="dict.dictLabel"
            :value="dict.dictValue"
          />
        </el-select>
      </el-form-item>
#elseif(($column.htmlType == "select" || $column.htmlType == "radio") && $dictType)
      <el-form-item label="${comment}" prop="${column.javaField}">
        <el-select v-model="queryParams.${column.javaField}" placeholder="请选择${comment}" clearable size="small">
          <el-option label="请选择字典生成" value="" />
        </el-select>
      </el-form-item>
#elseif($column.htmlType == "datetime" && $column.queryType != "BETWEEN")
      <el-form-item label="${comment}" prop="${column.javaField}">
        <el-date-picker clearable size="small"
          v-model="queryParams.${column.javaField}"
          type="date"
          value-format="yyyy-MM-dd"
          placeholder="选择${comment}">
        </el-date-picker>
      </el-form-item>
#elseif($column.htmlType == "datetime" && $column.queryType == "BETWEEN")
      <el-form-item label="${comment}">
        <el-date-picker
          v-model="daterange${AttrName}"
          size="small"
          style="width: 240px"
          value-format="yyyy-MM-dd"
          type="daterange"
          range-separator="-"
          start-placeholder="开始日期"
          end-placeholder="结束日期"
        ></el-date-picker>
      </el-form-item>
#end
#end
#end
      <el-form-item>
        <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
        <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">重置</el-button>
      </el-form-item>
    </el-form>

    <el-row :gutter="10" class="mb8">
      <el-col :span="1.5">
        <el-button
          type="primary"
          plain
          icon="el-icon-plus"
          size="mini"
          @click="handleAdd"
          v-hasPermi="[''${moduleName}:${businessName}:add'']"
        >新增</el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="success"
          plain
          icon="el-icon-edit"
          size="mini"
          :disabled="single"
          @click="handleUpdate"
          v-hasPermi="[''${moduleName}:${businessName}:edit'']"
        >修改</el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="danger"
          plain
          icon="el-icon-delete"
          size="mini"
          :disabled="multiple"
          @click="handleDelete"
          v-hasPermi="[''${moduleName}:${businessName}:remove'']"
        >删除</el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="warning"
          plain
          icon="el-icon-download"
          size="mini"
          @click="handleExport"
          v-hasPermi="[''${moduleName}:${businessName}:export'']"
        >导出</el-button>
      </el-col>
      <right-toolbar :showSearch.sync="showSearch" @queryTable="getList"></right-toolbar>
    </el-row>

    <el-table v-loading="loading" :data="${businessName}List" @selection-change="handleSelectionChange">
      <el-table-column type="selection" width="55" align="center" />
#foreach($column in $columns)
#set($javaField=$column.javaField)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($column.pk)
      <el-table-column label="${comment}" align="center" prop="${javaField}" />
#elseif($column.list && $column.htmlType == "datetime")
      <el-table-column label="${comment}" align="center" prop="${javaField}" width="180">
        <template slot-scope="scope">
          <span>{{ parseTime(scope.row.${javaField}, ''{y}-{m}-{d}'') }}</span>
        </template>
      </el-table-column>
#elseif($column.list && "" != $column.dictType)
      <el-table-column label="${comment}" align="center" prop="${javaField}" :formatter="${javaField}Format" />
#elseif($column.list && "" != $javaField)
      <el-table-column label="${comment}" align="center" prop="${javaField}" />
#end
#end
      <el-table-column label="操作" align="center" class-name="small-padding fixed-width">
        <template slot-scope="scope">
          <el-button
            size="mini"
            type="text"
            icon="el-icon-edit"
            @click="handleUpdate(scope.row)"
            v-hasPermi="[''${moduleName}:${businessName}:edit'']"
          >修改</el-button>
          <el-button
            size="mini"
            type="text"
            icon="el-icon-delete"
            @click="handleDelete(scope.row)"
            v-hasPermi="[''${moduleName}:${businessName}:remove'']"
          >删除</el-button>
        </template>
      </el-table-column>
    </el-table>

    <pagination
      v-show="total>0"
      :total="total"
      :page.sync="queryParams.pageNum"
      :limit.sync="queryParams.pageSize"
      @pagination="getList"
    />

    <!-- 添加或修改${functionName}对话框 -->
    <el-dialog :title="title" :visible.sync="open" width="500px" append-to-body>
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
#foreach($column in $columns)
#set($field=$column.javaField)
#if($column.insert && !$column.pk)
#if(($column.usableColumn) || (!$column.superColumn))
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#set($dictType=$column.dictType)
#if($column.htmlType == "input")
        <el-form-item label="${comment}" prop="${field}">
          <el-input v-model="form.${field}" placeholder="请输入${comment}" />
        </el-form-item>
#elseif($column.htmlType == "imageUpload")
        <el-form-item label="${comment}">
          <imageUpload v-model="form.${field}"/>
        </el-form-item>
#elseif($column.htmlType == "fileUpload")
        <el-form-item label="${comment}">
          <fileUpload v-model="form.${field}"/>
        </el-form-item>
#elseif($column.htmlType == "editor")
        <el-form-item label="${comment}">
          <editor v-model="form.${field}" :min-height="192"/>
        </el-form-item>
#elseif($column.htmlType == "select" && "" != $dictType)
        <el-form-item label="${comment}" prop="${field}">
          <el-select v-model="form.${field}" placeholder="请选择${comment}">
            <el-option
              v-for="dict in ${field}Options"
              :key="dict.dictValue"
              :label="dict.dictLabel"
              #if($column.javaType == "Integer" || $column.javaType == "Long"):value="parseInt(dict.dictValue)"#else:value="dict.dictValue"#end

            ></el-option>
          </el-select>
        </el-form-item>
#elseif($column.htmlType == "select" && $dictType)
        <el-form-item label="${comment}" prop="${field}">
          <el-select v-model="form.${field}" placeholder="请选择${comment}">
            <el-option label="请选择字典生成" value="" />
          </el-select>
        </el-form-item>
#elseif($column.htmlType == "checkbox" && "" != $dictType)
        <el-form-item label="${comment}">
          <el-checkbox-group v-model="form.${field}">
            <el-checkbox
              v-for="dict in ${field}Options"
              :key="dict.dictValue"
              :label="dict.dictValue">
              {{dict.dictLabel}}
            </el-checkbox>
          </el-checkbox-group>
        </el-form-item>
#elseif($column.htmlType == "checkbox" && $dictType)
        <el-form-item label="${comment}">
          <el-checkbox-group v-model="form.${field}">
            <el-checkbox>请选择字典生成</el-checkbox>
          </el-checkbox-group>
        </el-form-item>
#elseif($column.htmlType == "radio" && "" != $dictType)
        <el-form-item label="${comment}">
          <el-radio-group v-model="form.${field}">
            <el-radio
              v-for="dict in ${field}Options"
              :key="dict.dictValue"
              #if($column.javaType == "Integer" || $column.javaType == "Long"):label="parseInt(dict.dictValue)"#else:label="dict.dictValue"#end

            >{{dict.dictLabel}}</el-radio>
          </el-radio-group>
        </el-form-item>
#elseif($column.htmlType == "radio" && $dictType)
        <el-form-item label="${comment}">
          <el-radio-group v-model="form.${field}">
            <el-radio label="1">请选择字典生成</el-radio>
          </el-radio-group>
        </el-form-item>
#elseif($column.htmlType == "datetime")
        <el-form-item label="${comment}" prop="${field}">
          <el-date-picker clearable size="small"
            v-model="form.${field}"
            type="date"
            value-format="yyyy-MM-dd"
            placeholder="选择${comment}">
          </el-date-picker>
        </el-form-item>
#elseif($column.htmlType == "textarea")
        <el-form-item label="${comment}" prop="${field}">
          <el-input v-model="form.${field}" type="textarea" placeholder="请输入内容" />
        </el-form-item>
#end
#end
#end
#end
#if($table.sub)
        <el-divider content-position="center">${subTable.functionName}信息</el-divider>
        <el-row :gutter="10" class="mb8">
          <el-col :span="1.5">
            <el-button type="primary" icon="el-icon-plus" size="mini" @click="handleAdd${subClassName}">添加</el-button>
          </el-col>
          <el-col :span="1.5">
            <el-button type="danger" icon="el-icon-delete" size="mini" @click="handleDelete${subClassName}">删除</el-button>
          </el-col>
        </el-row>
        <el-table :data="${subclassName}List" :row-class-name="row${subClassName}Index" @selection-change="handle${subClassName}SelectionChange" ref="${subclassName}">
          <el-table-column type="selection" width="50" align="center" />
          <el-table-column label="序号" align="center" prop="index" width="50"/>
#foreach($column in $subTable.columns)
#set($javaField=$column.javaField)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if($column.pk || $javaField == ${subTableFkclassName})
#elseif($column.list && "" != $javaField)
          <el-table-column label="$comment" prop="${javaField}">
            <template slot-scope="scope">
              <el-input v-model="scope.row.$javaField" placeholder="请输入$comment" />
            </template>
          </el-table-column>
#end
#end
        </el-table>
#end
      </el-form>
      <div slot="footer" class="dialog-footer">
        <el-button type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
  </div>
</template>

<script>
    import {
        add${BusinessName},
        del${BusinessName},
        get${BusinessName},
        list${BusinessName},
        update${BusinessName}
    } from "@/api/${moduleName}/${businessName}";
    import ImageUpload from ''@/components/ImageUpload'';
    import FileUpload from ''@/components/FileUpload'';
    import Editor from ''@/components/Editor'';
        #foreach($column in $columns)
#if($column.insert && !$column.superColumn && !$column.pk && $column.htmlType == "imageUpload")
#break
#end
#end
#foreach($column in $columns)
#if($column.insert && !$column.superColumn && !$column.pk && $column.htmlType == "fileUpload")
#break
#end
#end
#foreach($column in $columns)
#if($column.insert && !$column.superColumn && !$column.pk && $column.htmlType == "editor")
#break
#end
#end

export default {
  name: "${BusinessName}",
  components: {
#foreach($column in $columns)
#if($column.insert && !$column.superColumn && !$column.pk && $column.htmlType == "imageUpload")
    ImageUpload,
#break
#end
#end
#foreach($column in $columns)
#if($column.insert && !$column.superColumn && !$column.pk && $column.htmlType == "fileUpload")
    FileUpload,
#break
#end
#end
#foreach($column in $columns)
#if($column.insert && !$column.superColumn && !$column.pk && $column.htmlType == "editor")
    Editor,
#break
#end
#end
  },
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
#if($table.sub)
      // 子表选中数据
      checked${subClassName}: [],
#end
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 显示搜索条件
      showSearch: true,
      // 总条数
      total: 0,
      // ${functionName}表格数据
      ${businessName}List: [],
#if($table.sub)
      // ${subTable.functionName}表格数据
      ${subclassName}List: [],
#end
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
#foreach ($column in $columns)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
#if(${column.dictType} != '''')
      // $comment字典
      ${column.javaField}Options: [],
#elseif($column.htmlType == "datetime" && $column.queryType == "BETWEEN")
#set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
      // $comment时间范围
      daterange${AttrName}: [],
#end
#end
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,
#foreach ($column in $columns)
#if($column.query)
        $column.javaField: null#if($velocityCount != $columns.size()),#end

#end
#end
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: {
#foreach ($column in $columns)
#if($column.required)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
        $column.javaField: [
          { required: true, message: "$comment不能为空", trigger: #if($column.htmlType == "select")"change"#else"blur"#end }
        ]#if($velocityCount != $columns.size()),#end

#end
#end
      }
    };
  },
  created() {
    this.getList();
#foreach ($column in $columns)
#if(${column.dictType} != '''')
    this.getDicts("${column.dictType}").then(response => {
      this.${column.javaField}Options = response.data;
    });
#end
#end
  },
  methods: {
    /** 查询${functionName}列表 */
    getList() {
      this.loading = true;
#foreach ($column in $columns)
#if($column.htmlType == "datetime" && $column.queryType == "BETWEEN")
      this.queryParams.params = {};
#break
#end
#end
#foreach ($column in $columns)
#if($column.htmlType == "datetime" && $column.queryType == "BETWEEN")
#set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
      if (null != this.daterange${AttrName} && '''' != this.daterange${AttrName}) {
        this.queryParams.params["begin${AttrName}"] = this.daterange${AttrName}[0];
        this.queryParams.params["end${AttrName}"] = this.daterange${AttrName}[1];
      }
#end
#end
      list${BusinessName}(this.queryParams).then(response => {
        this.${businessName}List = response.rows;
        this.total = response.total;
        this.loading = false;
      });
    },
#foreach ($column in $columns)
#if(${column.dictType} != '''')
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
#set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
#set($comment=$column.columnComment)
#end
    // $comment字典翻译
    ${column.javaField}Format(row, column) {
      return this.selectDictLabel#if($column.htmlType == "checkbox")s#end(this.${column.javaField}Options, row.${column.javaField});
    },
#end
#end
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
#foreach ($column in $columns)
#if($column.htmlType == "radio")
        $column.javaField: #if($column.javaType == "Integer" || $column.javaType == "Long")0#else"0"#end#if($velocityCount != $columns.size()),#end

#elseif($column.htmlType == "checkbox")
        $column.javaField: []#if($velocityCount != $columns.size()),#end

#else
        $column.javaField: null#if($velocityCount != $columns.size()),#end

#end
#end
      };
#if($table.sub)
      this.${subclassName}List = [];
#end
      this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
#foreach ($column in $columns)
#if($column.htmlType == "datetime" && $column.queryType == "BETWEEN")
#set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
      this.daterange${AttrName} = [];
#end
#end
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.${pkColumn.javaField})
      this.single = selection.length!==1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset();
      this.open = true;
      this.title = "添加${functionName}";
    },
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      const ${pkColumn.javaField} = row.${pkColumn.javaField} || this.ids
      get${BusinessName}(${pkColumn.javaField}).then(response => {
        this.form = response.data;
#foreach ($column in $columns)
#if($column.htmlType == "checkbox")
        this.form.$column.javaField = this.form.${column.javaField}.split(",");
#end
#end
#if($table.sub)
        this.${subclassName}List = response.data.${subclassName}List;
#end
        this.open = true;
        this.title = "修改${functionName}";
      });
    },
    /** 提交按钮 */
    submitForm() {
      this.#[[$]]#refs["form"].validate(valid => {
        if (valid) {
#foreach ($column in $columns)
#if($column.htmlType == "checkbox")
          this.form.$column.javaField = this.form.${column.javaField}.join(",");
#end
#end
#if($table.sub)
          this.form.${subclassName}List = this.${subclassName}List;
#end
          if (this.form.${pkColumn.javaField} != null) {
            update${BusinessName}(this.form).then(response => {
              this.msgSuccess("修改成功");
              this.open = false;
              this.getList();
            });
          } else {
            add${BusinessName}(this.form).then(response => {
              this.msgSuccess("新增成功");
              this.open = false;
              this.getList();
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const ${pkColumn.javaField}s = row.${pkColumn.javaField} || this.ids;
      this.$confirm(''是否确认删除${functionName}编号为"'' + ${pkColumn.javaField}s + ''"的数据项?'', "警告", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function() {
          return del${BusinessName}(${pkColumn.javaField}s);
        }).then(() => {
          this.getList();
          this.msgSuccess("删除成功");
        })
    },
#if($table.sub)
	/** ${subTable.functionName}序号 */
    row${subClassName}Index({ row, rowIndex }) {
      row.index = rowIndex + 1;
    },
    /** ${subTable.functionName}添加按钮操作 */
    handleAdd${subClassName}() {
      let obj = {};
#foreach($column in $subTable.columns)
#if($column.pk || $column.javaField == ${subTableFkclassName})
#elseif($column.list && "" != $javaField)
      obj.$column.javaField = "";
#end
#end
      this.${subclassName}List.push(obj);
    },
    /** ${subTable.functionName}删除按钮操作 */
    handleDelete${subClassName}() {
      if (this.checked${subClassName}.length == 0) {
        this.$alert("请先选择要删除的${subTable.functionName}数据", "提示", { confirmButtonText: "确定", });
      } else {
        this.${subclassName}List.splice(this.checked${subClassName}[0].index - 1, 1);
      }
    },
    /** 单选框选中数据 */
    handle${subClassName}SelectionChange(selection) {
      if (selection.length > 1) {
        this.$refs.${subclassName}.clearSelection();
        this.$refs.${subclassName}.toggleRowSelection(selection.pop());
      } else {
        this.checked${subClassName} = selection;
      }
    },
#end
    /** 导出按钮操作 */
    handleExport() {
      this.download(''${moduleName}/${businessName}/export'', {
        ...this.queryParams
      }, `${moduleName}_${businessName}.xlsx`)
    }
  }
};
</script>', NULL, 'index.vue.vm'
       , 'vue/views/#{moduleName}/#{businessName}/index.vue', NULL, '2021-11-22 09:46:07', NULL, '2021-11-22 09:46:07'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (14, '模板属性说明', '## 模板变量信息说明 --
## ${tplCategory} 生成的类型
## ${tableName} 数据表名称
## ${functionName} 程序名称
## ${ClassName} 类名-首字母大写
## ${className} 类名-首字母小写
## ${moduleName} 模块名称
## ${BusinessName} 业务名称 -首字母大写
## ${businessName} 业务名称 -首字母小写
## ${basePackage} 包前缀 如 xxx.xx.x 的 xxx
## ${packageName} 包路径
## ${author} 作者
## ${datetime} 当前时间-模板运行时的时间
## ${pkColumn} 主键字段
## ${importList} 需要导入包的列表
## 如果字段类型包含 LOCAL_DATE_TIME 则导入 java.time.LocalDateTime com.fasterxml.jackson.annotation.JsonFormat
## 如果所有列包含BigDecimal 则导入java.math.BigDecimal
## 如果生成的类型是是数据结构则导入 java.util.List
## ${permissionPrefix} 更具模块名及业务名自动生成的权限字符   moduleName：businessName：
## ${columns} 表的所有字段集合
## ${isQuery} 是否生成更新代码
## ${isInsert} 是否生成插入代码
## ${isUpdate} 是否生成更新代码
## ${isRemove} 是否生成删除代码
## ${isWrapper} 是否生成Wrapper代码
## ${isManager} 是否生成manager层级

## ${table} GenTable 表对象属性说明

##  编号  tableId
##  表名称  tableName
##  表描述  tableComment
##  关联父表的表名  subTableName
##  本表关联父表的外键名  subTableFkName
##  实体类名称(首字母大写) className
##  生成的类型（crud单表操作 tree树表操作 sub主子表操作） tplCategory
##  生成包路径  packageName
##  生成模块名 moduleName
##  生成业务名  businessName
##  生成功能名  functionName
##  作者  functionAuthor
##  生成代码方式（0zip压缩包 1自定义路径）  genType
##  生成路径（不填默认项目路径） genPath
##  主键信息  类型 GenTableColumn  pkColumn
##  子表信息  类型 GenTable subTable
##  表列信息 List<GenTableColumn> columns
##  其它生成选项  options
##  树编码字段  treeCode
##  树父编码字段  treeParentCode
##  树名称字段  treeName
##  上级菜单ID字段 parentMenuId
##  级菜单名称字段 parentMenuName
##  是否生成查询 isQuery
##  是否生成插入  isInsert
##  是否生成更新  isUpdate
##  是否生成删除  isRemove
##  是否生成Lambda条件语句 isWrapper
##  是否生成Manager层 isManager
##  使用的模板id  templateIds

## ${table.pkColumn} GenTableColumn 主键信息属性说明

 ##     编号 columnId
 ##    归属表编号 tableId
 ##    列名称  columnName
 ##    列描述  columnComment
 ##    列类型  columnType
 ##    JAVA类型   javaType
 ##    JAVA字段名  javaField
 ##    是否主键（1是） isPk
 ##    是否必填（1是） isRequired
 ##    是否为插入字段（1是）  isInsert
 ##    是否编辑字段（1是）   isEdit
 ##    是否列表字段（1是）   isList
 ##    是否查询字段（1是）   isQuery
 ##    查询方式 queryType（EQ等于、NE不等于、GT大于、LT小于、LIKE模糊、BETWEEN范围）
 ##    显示类型 htmlType（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件、image图片上传控件、upload文件上传控件、editor富文本控件）
 ##    字典类型 dictType
 ##    排序   sort', NULL, 'demo.vm'
       , '/', NULL, '2021-11-22 17:34:20', NULL, '2021-11-22 17:34:20'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (15, '数据库文档', '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<pkg:package xmlns:pkg="http://schemas.microsoft.com/office/2006/xmlPackage">
    <pkg:part pkg:name="/_rels/.rels" pkg:contentType="application/vnd.openxmlformats-package.relationships+xml"
              pkg:padding="512">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties"
                              Target="docProps/app.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties"
                              Target="docProps/core.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument"
                              Target="word/document.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/document.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml">
        <pkg:xmlData>
            <w:document xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                        xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:body>
                    #foreach ($t in $tables)
                        <w:p w14:paraId="14105319" w14:textId="77777777" w:rsidR="009A0552" w:rsidRDefault="009A0552"
                             w:rsidP="009A0552">
                            <w:pPr>
                                <w:pStyle w:val="2"/>
                                <w:numPr>
                                    <w:ilvl w:val="1"/>
                                    <w:numId w:val="1"/>
                                </w:numPr>
                                <w:tabs>
                                    <w:tab w:val="num" w:pos="360"/>
                                </w:tabs>
                                <w:ind w:left="0" w:firstLine="0"/>
                                <w:rPr>
                                    <w:lang w:eastAsia="zh-CN"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                    <w:lang w:eastAsia="zh-CN"/>
                                </w:rPr>
                                <w:t>${t.tableComment}</w:t>
                            </w:r>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                    <w:lang w:eastAsia="zh-CN"/>
                                </w:rPr>
                                <w:t>表</w:t>
                            </w:r>
                        </w:p>
                        <w:tbl>
                            <w:tblPr>
                                <w:tblW w:w="8521" w:type="dxa"/>
                                <w:tblInd w:w="113" w:type="dxa"/>
                                <w:tblBorders>
                                    <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                    <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
                                </w:tblBorders>
                                <w:tblLayout w:type="fixed"/>
                                <w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0"
                                           w:noHBand="0" w:noVBand="1"/>
                            </w:tblPr>
                            <w:tblGrid>
                                <w:gridCol w:w="715"/>
                                <w:gridCol w:w="1548"/>
                                <w:gridCol w:w="1436"/>
                                <w:gridCol w:w="1825"/>
                                <w:gridCol w:w="1275"/>
                                <w:gridCol w:w="1011"/>
                                <w:gridCol w:w="711"/>
                            </w:tblGrid>
                            <w:tr w:rsidR="009A0552" w:rsidRPr="00795EEA" w14:paraId="2241E7CC" w14:textId="77777777"
                                  w:rsidTr="007E2347">
                                <w:trPr>
                                    <w:trHeight w:val="301"/>
                                    <w:tblHeader/>
                                </w:trPr>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="2263" w:type="dxa"/>
                                        <w:gridSpan w:val="2"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="1DE04073" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>逻辑表名</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="3261" w:type="dxa"/>
                                        <w:gridSpan w:val="2"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="3F153818" w14:textId="04F8C5F9" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>${t.tableComment}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1275" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="2C15D307" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>物理表名 </w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1722" w:type="dxa"/>
                                        <w:gridSpan w:val="2"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="5C4D7301" w14:textId="758F58A3" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>${t.tableName}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                            </w:tr>
                            <w:tr w:rsidR="009A0552" w:rsidRPr="00795EEA" w14:paraId="01A74073" w14:textId="77777777"
                                  w:rsidTr="007E2347">
                                <w:trPr>
                                    <w:trHeight w:val="301"/>
                                    <w:tblHeader/>
                                </w:trPr>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="2263" w:type="dxa"/>
                                        <w:gridSpan w:val="2"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="55664F4E" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>主键</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="3261" w:type="dxa"/>
                                        <w:gridSpan w:val="2"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="50FD4724" w14:textId="70A64471" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>${t.pkColumn.columnName}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1275" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="1F9F07D2" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>索引</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1722" w:type="dxa"/>
                                        <w:gridSpan w:val="2"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="32FBD1AA" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>#foreach ($index in $t.indexNames) ${index} #end</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                            </w:tr>
                            <w:tr w:rsidR="009A0552" w:rsidRPr="00795EEA" w14:paraId="4066F4F8" w14:textId="77777777"
                                  w:rsidTr="007E2347">
                                <w:trPr>
                                    <w:trHeight w:val="301"/>
                                    <w:tblHeader/>
                                </w:trPr>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="715" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="08603F84" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>序号</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1548" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="6F716638" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>逻辑字段名</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1436" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="6C6B71AF" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>物理字段名</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1825" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="70F5280E" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>数据类型</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1275" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="080E8711" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>空值</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1011" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="306052E5" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>默认值</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="711" w:type="dxa"/>
                                        <w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="7A58EC1C" w14:textId="77777777" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="009A0552" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r w:rsidRPr="00795EEA">
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>备注</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                            </w:tr>#foreach ($c in $t.columns)
                            <w:tr w:rsidR="009A0552" w:rsidRPr="00795EEA" w14:paraId="724659EE" w14:textId="77777777"
                                  w:rsidTr="007E2347">
                                <w:trPr>
                                    <w:trHeight w:val="301"/>
                                </w:trPr>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="715" w:type="dxa"/>
                                        <w:vAlign w:val="center"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="48D09F3E" w14:textId="16307F2A" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="00695CAC">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:jc w:val="both"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>$velocityCount</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1548" w:type="dxa"/>
                                        <w:vAlign w:val="center"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="0679DC80" w14:textId="686363B8" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="00695CAC">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:jc w:val="both"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t xml:space="preserve"> </w:t>
                                        </w:r>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t xml:space="preserve"> </w:t>
                                        </w:r>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>${c.columnComment}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1436" w:type="dxa"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="0C0CDF36" w14:textId="097CEEA1" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>${c.columnName}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1825" w:type="dxa"/>
                                        <w:vAlign w:val="center"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="64DBE3BF" w14:textId="22442337" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>${c.columnType}</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1275" w:type="dxa"/>
                                        <w:vAlign w:val="center"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="62BCFB03" w14:textId="222152AE" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>#if($c.isRequired == 1) 否 #else 是 #end</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="1011" w:type="dxa"/>
                                        <w:vAlign w:val="center"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="06F6A01F" w14:textId="27265A53" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="007E2347">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t>#if($c.defaultValue)$c.defaultValue#end</w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                                <w:tc>
                                    <w:tcPr>
                                        <w:tcW w:w="711" w:type="dxa"/>
                                    </w:tcPr>
                                    <w:p w14:paraId="3E698CAB" w14:textId="6401721E" w:rsidR="009A0552" w:rsidRPr="00795EEA"
                                         w:rsidRDefault="00695CAC" w:rsidP="00695CAC">
                                        <w:pPr>
                                            <w:pStyle w:val="w"/>
                                            <w:spacing w:line="360" w:lineRule="auto"/>
                                            <w:jc w:val="both"/>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                        </w:pPr>
                                        <w:r>
                                            <w:rPr>
                                                <w:rFonts w:asciiTheme="minorEastAsia" w:eastAsiaTheme="minorEastAsia"
                                                          w:hAnsiTheme="minorEastAsia" w:hint="eastAsia"/>
                                                <w:sz w:val="24"/>
                                                <w:szCs w:val="24"/>
                                            </w:rPr>
                                            <w:t xml:space="preserve"></w:t>
                                        </w:r>
                                    </w:p>
                                </w:tc>
                            </w:tr>#end
                        </w:tbl>#end
                    <w:p w14:paraId="694D076D" w14:textId="77777777" w:rsidR="000D1916" w:rsidRDefault="00EE5595">
                        <w:pPr>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                            </w:rPr>
                        </w:pPr>
                    </w:p>
                    <w:sectPr w:rsidR="000D1916">
                        <w:pgSz w:w="11906" w:h="16838"/>
                        <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992"
                                 w:gutter="0"/>
                        <w:cols w:space="425"/>
                        <w:docGrid w:type="lines" w:linePitch="312"/>
                    </w:sectPr>
                </w:body>
            </w:document>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/_rels/document.xml.rels"
              pkg:contentType="application/vnd.openxmlformats-package.relationships+xml" pkg:padding="256">
        <pkg:xmlData>
            <Relationships xmlns="http://schemas.openxmlformats.org/package/2006/relationships">
                <Relationship Id="rId8" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme"
                              Target="theme/theme1.xml"/>
                <Relationship Id="rId3"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings"
                              Target="settings.xml"/>
                <Relationship Id="rId7"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable"
                              Target="fontTable.xml"/>
                <Relationship Id="rId2"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles"
                              Target="styles.xml"/>
                <Relationship Id="rId1"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering"
                              Target="numbering.xml"/>
                <Relationship Id="rId6"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/endnotes"
                              Target="endnotes.xml"/>
                <Relationship Id="rId5"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/footnotes"
                              Target="footnotes.xml"/>
                <Relationship Id="rId4"
                              Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings"
                              Target="webSettings.xml"/>
            </Relationships>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/footnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml">
        <pkg:xmlData>
            <w:footnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                         xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:footnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="00A98FDA" w14:textId="77777777" w:rsidR="00EE5595" w:rsidRDefault="00EE5595"
                         w:rsidP="009A0552">
                        <w:pPr>
                            <w:spacing w:line="240" w:lineRule="auto"/>
                        </w:pPr>
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:footnote>
                <w:footnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="1CDC730E" w14:textId="77777777" w:rsidR="00EE5595" w:rsidRDefault="00EE5595"
                         w:rsidP="009A0552">
                        <w:pPr>
                            <w:spacing w:line="240" w:lineRule="auto"/>
                        </w:pPr>
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:footnote>
            </w:footnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/endnotes.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml">
        <pkg:xmlData>
            <w:endnotes xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                        xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                        xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                        xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                        xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                        xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                        xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                        xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                        xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                        xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                        xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml"
                        xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                        xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                        xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                        xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                        xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                        xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                        xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                        mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:endnote w:type="separator" w:id="-1">
                    <w:p w14:paraId="5A361E7D" w14:textId="77777777" w:rsidR="00EE5595" w:rsidRDefault="00EE5595"
                         w:rsidP="009A0552">
                        <w:pPr>
                            <w:spacing w:line="240" w:lineRule="auto"/>
                        </w:pPr>
                        <w:r>
                            <w:separator/>
                        </w:r>
                    </w:p>
                </w:endnote>
                <w:endnote w:type="continuationSeparator" w:id="0">
                    <w:p w14:paraId="51D52676" w14:textId="77777777" w:rsidR="00EE5595" w:rsidRDefault="00EE5595"
                         w:rsidP="009A0552">
                        <w:pPr>
                            <w:spacing w:line="240" w:lineRule="auto"/>
                        </w:pPr>
                        <w:r>
                            <w:continuationSeparator/>
                        </w:r>
                    </w:p>
                </w:endnote>
            </w:endnotes>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/theme/theme1.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.theme+xml">
        <pkg:xmlData>
            <a:theme xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" name="Office 主题​​">
                <a:themeElements>
                    <a:clrScheme name="Office">
                        <a:dk1>
                            <a:sysClr val="windowText" lastClr="000000"/>
                        </a:dk1>
                        <a:lt1>
                            <a:sysClr val="window" lastClr="FFFFFF"/>
                        </a:lt1>
                        <a:dk2>
                            <a:srgbClr val="44546A"/>
                        </a:dk2>
                        <a:lt2>
                            <a:srgbClr val="E7E6E6"/>
                        </a:lt2>
                        <a:accent1>
                            <a:srgbClr val="4472C4"/>
                        </a:accent1>
                        <a:accent2>
                            <a:srgbClr val="ED7D31"/>
                        </a:accent2>
                        <a:accent3>
                            <a:srgbClr val="A5A5A5"/>
                        </a:accent3>
                        <a:accent4>
                            <a:srgbClr val="FFC000"/>
                        </a:accent4>
                        <a:accent5>
                            <a:srgbClr val="5B9BD5"/>
                        </a:accent5>
                        <a:accent6>
                            <a:srgbClr val="70AD47"/>
                        </a:accent6>
                        <a:hlink>
                            <a:srgbClr val="0563C1"/>
                        </a:hlink>
                        <a:folHlink>
                            <a:srgbClr val="954F72"/>
                        </a:folHlink>
                    </a:clrScheme>
                    <a:fontScheme name="Office">
                        <a:majorFont>
                            <a:latin typeface="等线 Light" panose="020F0302020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游ゴシック Light"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线 Light"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Times New Roman"/>
                            <a:font script="Hebr" typeface="Times New Roman"/>
                            <a:font script="Thai" typeface="Angsana New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="MoolBoran"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Times New Roman"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:majorFont>
                        <a:minorFont>
                            <a:latin typeface="等线" panose="020F0502020204030204"/>
                            <a:ea typeface=""/>
                            <a:cs typeface=""/>
                            <a:font script="Jpan" typeface="游明朝"/>
                            <a:font script="Hang" typeface="맑은 고딕"/>
                            <a:font script="Hans" typeface="等线"/>
                            <a:font script="Hant" typeface="新細明體"/>
                            <a:font script="Arab" typeface="Arial"/>
                            <a:font script="Hebr" typeface="Arial"/>
                            <a:font script="Thai" typeface="Cordia New"/>
                            <a:font script="Ethi" typeface="Nyala"/>
                            <a:font script="Beng" typeface="Vrinda"/>
                            <a:font script="Gujr" typeface="Shruti"/>
                            <a:font script="Khmr" typeface="DaunPenh"/>
                            <a:font script="Knda" typeface="Tunga"/>
                            <a:font script="Guru" typeface="Raavi"/>
                            <a:font script="Cans" typeface="Euphemia"/>
                            <a:font script="Cher" typeface="Plantagenet Cherokee"/>
                            <a:font script="Yiii" typeface="Microsoft Yi Baiti"/>
                            <a:font script="Tibt" typeface="Microsoft Himalaya"/>
                            <a:font script="Thaa" typeface="MV Boli"/>
                            <a:font script="Deva" typeface="Mangal"/>
                            <a:font script="Telu" typeface="Gautami"/>
                            <a:font script="Taml" typeface="Latha"/>
                            <a:font script="Syrc" typeface="Estrangelo Edessa"/>
                            <a:font script="Orya" typeface="Kalinga"/>
                            <a:font script="Mlym" typeface="Kartika"/>
                            <a:font script="Laoo" typeface="DokChampa"/>
                            <a:font script="Sinh" typeface="Iskoola Pota"/>
                            <a:font script="Mong" typeface="Mongolian Baiti"/>
                            <a:font script="Viet" typeface="Arial"/>
                            <a:font script="Uigh" typeface="Microsoft Uighur"/>
                            <a:font script="Geor" typeface="Sylfaen"/>
                            <a:font script="Armn" typeface="Arial"/>
                            <a:font script="Bugi" typeface="Leelawadee UI"/>
                            <a:font script="Bopo" typeface="Microsoft JhengHei"/>
                            <a:font script="Java" typeface="Javanese Text"/>
                            <a:font script="Lisu" typeface="Segoe UI"/>
                            <a:font script="Mymr" typeface="Myanmar Text"/>
                            <a:font script="Nkoo" typeface="Ebrima"/>
                            <a:font script="Olck" typeface="Nirmala UI"/>
                            <a:font script="Osma" typeface="Ebrima"/>
                            <a:font script="Phag" typeface="Phagspa"/>
                            <a:font script="Syrn" typeface="Estrangelo Edessa"/>
                            <a:font script="Syrj" typeface="Estrangelo Edessa"/>
                            <a:font script="Syre" typeface="Estrangelo Edessa"/>
                            <a:font script="Sora" typeface="Nirmala UI"/>
                            <a:font script="Tale" typeface="Microsoft Tai Le"/>
                            <a:font script="Talu" typeface="Microsoft New Tai Lue"/>
                            <a:font script="Tfng" typeface="Ebrima"/>
                        </a:minorFont>
                    </a:fontScheme>
                    <a:fmtScheme name="Office">
                        <a:fillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="110000"/>
                                            <a:satMod val="105000"/>
                                            <a:tint val="67000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="103000"/>
                                            <a:tint val="73000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="105000"/>
                                            <a:satMod val="109000"/>
                                            <a:tint val="81000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="103000"/>
                                            <a:lumMod val="102000"/>
                                            <a:tint val="94000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:satMod val="110000"/>
                                            <a:lumMod val="100000"/>
                                            <a:shade val="100000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:lumMod val="99000"/>
                                            <a:satMod val="120000"/>
                                            <a:shade val="78000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:fillStyleLst>
                        <a:lnStyleLst>
                            <a:ln w="6350" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="12700" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                            <a:ln w="19050" cap="flat" cmpd="sng" algn="ctr">
                                <a:solidFill>
                                    <a:schemeClr val="phClr"/>
                                </a:solidFill>
                                <a:prstDash val="solid"/>
                                <a:miter lim="800000"/>
                            </a:ln>
                        </a:lnStyleLst>
                        <a:effectStyleLst>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst/>
                            </a:effectStyle>
                            <a:effectStyle>
                                <a:effectLst>
                                    <a:outerShdw blurRad="57150" dist="19050" dir="5400000" algn="ctr" rotWithShape="0">
                                        <a:srgbClr val="000000">
                                            <a:alpha val="63000"/>
                                        </a:srgbClr>
                                    </a:outerShdw>
                                </a:effectLst>
                            </a:effectStyle>
                        </a:effectStyleLst>
                        <a:bgFillStyleLst>
                            <a:solidFill>
                                <a:schemeClr val="phClr"/>
                            </a:solidFill>
                            <a:solidFill>
                                <a:schemeClr val="phClr">
                                    <a:tint val="95000"/>
                                    <a:satMod val="170000"/>
                                </a:schemeClr>
                            </a:solidFill>
                            <a:gradFill rotWithShape="1">
                                <a:gsLst>
                                    <a:gs pos="0">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="93000"/>
                                            <a:satMod val="150000"/>
                                            <a:shade val="98000"/>
                                            <a:lumMod val="102000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="50000">
                                        <a:schemeClr val="phClr">
                                            <a:tint val="98000"/>
                                            <a:satMod val="130000"/>
                                            <a:shade val="90000"/>
                                            <a:lumMod val="103000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                    <a:gs pos="100000">
                                        <a:schemeClr val="phClr">
                                            <a:shade val="63000"/>
                                            <a:satMod val="120000"/>
                                        </a:schemeClr>
                                    </a:gs>
                                </a:gsLst>
                                <a:lin ang="5400000" scaled="0"/>
                            </a:gradFill>
                        </a:bgFillStyleLst>
                    </a:fmtScheme>
                </a:themeElements>
                <a:objectDefaults/>
                <a:extraClrSchemeLst/>
                <a:extLst>
                    <a:ext uri="{05A4C25C-085E-4340-85A3-A5531E510DB2}">
                        <thm15:themeFamily xmlns:thm15="http://schemas.microsoft.com/office/thememl/2012/main"
                                           name="Office Theme" id="{62F939B6-93AF-4DB8-9C6B-D6C7DFDC589F}"
                                           vid="{4A3C46E8-61CC-4603-A589-7422A47A8E4A}"/>
                    </a:ext>
                </a:extLst>
            </a:theme>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/settings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml">
        <pkg:xmlData>
            <w:settings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                        xmlns:o="urn:schemas-microsoft-com:office:office"
                        xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                        xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                        xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word"
                        xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                        xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                        xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                        xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                        xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                        xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                        xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                        xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                        xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main"
                        mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:zoom w:percent="120"/>
                <w:bordersDoNotSurroundHeader/>
                <w:bordersDoNotSurroundFooter/>
                <w:proofState w:spelling="clean" w:grammar="clean"/>
                <w:defaultTabStop w:val="420"/>
                <w:drawingGridVerticalSpacing w:val="156"/>
                <w:displayHorizontalDrawingGridEvery w:val="0"/>
                <w:displayVerticalDrawingGridEvery w:val="2"/>
                <w:characterSpacingControl w:val="compressPunctuation"/>
                <w:hdrShapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2050"/>
                </w:hdrShapeDefaults>
                <w:footnotePr>
                    <w:footnote w:id="-1"/>
                    <w:footnote w:id="0"/>
                </w:footnotePr>
                <w:endnotePr>
                    <w:endnote w:id="-1"/>
                    <w:endnote w:id="0"/>
                </w:endnotePr>
                <w:compat>
                    <w:spaceForUL/>
                    <w:balanceSingleByteDoubleByteWidth/>
                    <w:doNotLeaveBackslashAlone/>
                    <w:ulTrailSpace/>
                    <w:doNotExpandShiftReturn/>
                    <w:adjustLineHeightInTable/>
                    <w:useFELayout/>
                    <w:compatSetting w:name="compatibilityMode" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="15"/>
                    <w:compatSetting w:name="overrideTableStyleFontSizeAndJustification"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="enableOpenTypeFeatures" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="1"/>
                    <w:compatSetting w:name="doNotFlipMirrorIndents" w:uri="http://schemas.microsoft.com/office/word"
                                     w:val="1"/>
                    <w:compatSetting w:name="differentiateMultirowTableHeaders"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="1"/>
                    <w:compatSetting w:name="useWord2013TrackBottomHyphenation"
                                     w:uri="http://schemas.microsoft.com/office/word" w:val="0"/>
                </w:compat>
                <w:rsids>
                    <w:rsidRoot w:val="00220EEC"/>
                    <w:rsid w:val="001F2E0D"/>
                    <w:rsid w:val="00220EEC"/>
                    <w:rsid w:val="003A3E39"/>
                    <w:rsid w:val="003D4CE2"/>
                    <w:rsid w:val="00695CAC"/>
                    <w:rsid w:val="008C5754"/>
                    <w:rsid w:val="009A0552"/>
                    <w:rsid w:val="00EE5595"/>
                </w:rsids>
                <m:mathPr>
                    <m:mathFont m:val="Cambria Math"/>
                    <m:brkBin m:val="before"/>
                    <m:brkBinSub m:val="--"/>
                    <m:smallFrac m:val="0"/>
                    <m:dispDef/>
                    <m:lMargin m:val="0"/>
                    <m:rMargin m:val="0"/>
                    <m:defJc m:val="centerGroup"/>
                    <m:wrapIndent m:val="1440"/>
                    <m:intLim m:val="subSup"/>
                    <m:naryLim m:val="undOvr"/>
                </m:mathPr>
                <w:themeFontLang w:val="en-US" w:eastAsia="zh-CN"/>
                <w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1"
                                    w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5"
                                    w:accent6="accent6" w:hyperlink="hyperlink"
                                    w:followedHyperlink="followedHyperlink"/>
                <w:shapeDefaults>
                    <o:shapedefaults v:ext="edit" spidmax="2050"/>
                    <o:shapelayout v:ext="edit">
                        <o:idmap v:ext="edit" data="2"/>
                    </o:shapelayout>
                </w:shapeDefaults>
                <w:decimalSymbol w:val="."/>
                <w:listSeparator w:val=","/>
                <w14:docId w14:val="5EB4B75A"/>
                <w15:chartTrackingRefBased/>
                <w15:docId w15:val="{91683C09-5373-4386-807E-234BC1A8C628}"/>
            </w:settings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/numbering.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml">
        <pkg:xmlData>
            <w:numbering xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
                         xmlns:cx="http://schemas.microsoft.com/office/drawing/2014/chartex"
                         xmlns:cx1="http://schemas.microsoft.com/office/drawing/2015/9/8/chartex"
                         xmlns:cx2="http://schemas.microsoft.com/office/drawing/2015/10/21/chartex"
                         xmlns:cx3="http://schemas.microsoft.com/office/drawing/2016/5/9/chartex"
                         xmlns:cx4="http://schemas.microsoft.com/office/drawing/2016/5/10/chartex"
                         xmlns:cx5="http://schemas.microsoft.com/office/drawing/2016/5/11/chartex"
                         xmlns:cx6="http://schemas.microsoft.com/office/drawing/2016/5/12/chartex"
                         xmlns:cx7="http://schemas.microsoft.com/office/drawing/2016/5/13/chartex"
                         xmlns:cx8="http://schemas.microsoft.com/office/drawing/2016/5/14/chartex"
                         xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                         xmlns:aink="http://schemas.microsoft.com/office/drawing/2016/ink"
                         xmlns:am3d="http://schemas.microsoft.com/office/drawing/2017/model3d"
                         xmlns:o="urn:schemas-microsoft-com:office:office"
                         xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                         xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
                         xmlns:v="urn:schemas-microsoft-com:vml"
                         xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
                         xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
                         xmlns:w10="urn:schemas-microsoft-com:office:word"
                         xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                         xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                         xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                         xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                         xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                         xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                         xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                         xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                         xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
                         xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
                         xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
                         xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
                         mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh wp14">
                <w:abstractNum w:abstractNumId="0" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="71C97A17"/>
                    <w:multiLevelType w:val="multilevel"/>
                    <w:tmpl w:val="0409001F"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="425" w:hanging="425"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="1">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="567" w:hanging="567"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="2">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2.%3."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="709" w:hanging="709"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="3">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2.%3.%4."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="851" w:hanging="851"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="4">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2.%3.%4.%5."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="992" w:hanging="992"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="5">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2.%3.%4.%5.%6."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1134" w:hanging="1134"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="6">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2.%3.%4.%5.%6.%7."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1276" w:hanging="1276"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="7">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2.%3.%4.%5.%6.%7.%8."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1418" w:hanging="1418"/>
                        </w:pPr>
                    </w:lvl>
                    <w:lvl w:ilvl="8">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:lvlText w:val="%1.%2.%3.%4.%5.%6.%7.%8.%9."/>
                        <w:lvlJc w:val="left"/>
                        <w:pPr>
                            <w:ind w:left="1559" w:hanging="1559"/>
                        </w:pPr>
                    </w:lvl>
                </w:abstractNum>
                <w:abstractNum w:abstractNumId="1" w15:restartNumberingAfterBreak="0">
                    <w:nsid w:val="73163015"/>
                    <w:multiLevelType w:val="singleLevel"/>
                    <w:tmpl w:val="73163015"/>
                    <w:lvl w:ilvl="0">
                        <w:start w:val="1"/>
                        <w:numFmt w:val="decimal"/>
                        <w:suff w:val="space"/>
                        <w:lvlText w:val="%1."/>
                        <w:lvlJc w:val="left"/>
                    </w:lvl>
                </w:abstractNum>
                <w:num w:numId="1">
                    <w:abstractNumId w:val="0"/>
                </w:num>
                <w:num w:numId="2">
                    <w:abstractNumId w:val="1"/>
                </w:num>
            </w:numbering>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/styles.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml">
        <pkg:xmlData>
            <w:styles xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                      xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                      xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                      xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                      xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                      xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                      xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                      xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                      xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                      xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                      mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:docDefaults>
                    <w:rPrDefault>
                        <w:rPr>
                            <w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorEastAsia"
                                      w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/>
                            <w:kern w:val="2"/>
                            <w:sz w:val="21"/>
                            <w:szCs w:val="22"/>
                            <w:lang w:val="en-US" w:eastAsia="zh-CN" w:bidi="ar-SA"/>
                        </w:rPr>
                    </w:rPrDefault>
                    <w:pPrDefault/>
                </w:docDefaults>
                <w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="0" w:defUnhideWhenUsed="0"
                                w:defQFormat="0" w:count="376">
                    <w:lsdException w:name="Normal" w:uiPriority="0" w:qFormat="1"/>
                    <w:lsdException w:name="heading 1" w:uiPriority="9" w:qFormat="1"/>
                    <w:lsdException w:name="heading 2" w:semiHidden="1" w:uiPriority="0" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 3" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 4" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 5" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 6" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 7" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 8" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="heading 9" w:semiHidden="1" w:uiPriority="9" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="index 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index 9" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 1" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 2" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 3" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 4" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 5" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 6" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 7" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 8" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toc 9" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footer" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="index heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="caption" w:semiHidden="1" w:uiPriority="35" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="table of figures" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="envelope return" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="footnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="line number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="page number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote reference" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="endnote text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="table of authorities" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="macro" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="toa heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Bullet 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Number 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Title" w:uiPriority="10" w:qFormat="1"/>
                    <w:lsdException w:name="Closing" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Default Paragraph Font" w:semiHidden="1" w:uiPriority="1"
                                    w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="List Continue 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Message Header" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Subtitle" w:uiPriority="11" w:qFormat="1"/>
                    <w:lsdException w:name="Salutation" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Date" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text First Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Note Heading" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Body Text Indent 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Block Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="FollowedHyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Strong" w:uiPriority="22" w:qFormat="1"/>
                    <w:lsdException w:name="Emphasis" w:uiPriority="20" w:qFormat="1"/>
                    <w:lsdException w:name="Document Map" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Plain Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="E-mail Signature" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Top of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Bottom of Form" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal (Web)" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Acronym" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Address" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Cite" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Code" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Definition" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Keyboard" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Preformatted" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Sample" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Typewriter" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="HTML Variable" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Normal Table" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="annotation subject" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="No List" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Outline List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Simple 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Classic 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Colorful 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Columns 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 4" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 5" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 6" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 7" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table List 8" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table 3D effects 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Contemporary" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Elegant" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Professional" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Subtle 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 1" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 2" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Web 3" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Balloon Text" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Table Grid" w:uiPriority="39"/>
                    <w:lsdException w:name="Table Theme" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Placeholder Text" w:semiHidden="1"/>
                    <w:lsdException w:name="No Spacing" w:uiPriority="1" w:qFormat="1"/>
                    <w:lsdException w:name="Light Shading" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 1" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 1" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 1" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 1" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 1" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 1" w:uiPriority="65"/>
                    <w:lsdException w:name="Revision" w:semiHidden="1"/>
                    <w:lsdException w:name="List Paragraph" w:uiPriority="34" w:qFormat="1"/>
                    <w:lsdException w:name="Quote" w:uiPriority="29" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Quote" w:uiPriority="30" w:qFormat="1"/>
                    <w:lsdException w:name="Medium List 2 Accent 1" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 1" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 1" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 1" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 1" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 1" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 1" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 1" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 2" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 2" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 2" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 2" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 2" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 2" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 2" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 2" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 2" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 2" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 2" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 2" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 2" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 2" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 3" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 3" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 3" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 3" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 3" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 3" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 3" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 3" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 3" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 3" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 3" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 3" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 3" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 3" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 4" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 4" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 4" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 4" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 4" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 4" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 4" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 4" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 4" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 4" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 4" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 4" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 4" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 4" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 5" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 5" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 5" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 5" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 5" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 5" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 5" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 5" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 5" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 5" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 5" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 5" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 5" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 5" w:uiPriority="73"/>
                    <w:lsdException w:name="Light Shading Accent 6" w:uiPriority="60"/>
                    <w:lsdException w:name="Light List Accent 6" w:uiPriority="61"/>
                    <w:lsdException w:name="Light Grid Accent 6" w:uiPriority="62"/>
                    <w:lsdException w:name="Medium Shading 1 Accent 6" w:uiPriority="63"/>
                    <w:lsdException w:name="Medium Shading 2 Accent 6" w:uiPriority="64"/>
                    <w:lsdException w:name="Medium List 1 Accent 6" w:uiPriority="65"/>
                    <w:lsdException w:name="Medium List 2 Accent 6" w:uiPriority="66"/>
                    <w:lsdException w:name="Medium Grid 1 Accent 6" w:uiPriority="67"/>
                    <w:lsdException w:name="Medium Grid 2 Accent 6" w:uiPriority="68"/>
                    <w:lsdException w:name="Medium Grid 3 Accent 6" w:uiPriority="69"/>
                    <w:lsdException w:name="Dark List Accent 6" w:uiPriority="70"/>
                    <w:lsdException w:name="Colorful Shading Accent 6" w:uiPriority="71"/>
                    <w:lsdException w:name="Colorful List Accent 6" w:uiPriority="72"/>
                    <w:lsdException w:name="Colorful Grid Accent 6" w:uiPriority="73"/>
                    <w:lsdException w:name="Subtle Emphasis" w:uiPriority="19" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Emphasis" w:uiPriority="21" w:qFormat="1"/>
                    <w:lsdException w:name="Subtle Reference" w:uiPriority="31" w:qFormat="1"/>
                    <w:lsdException w:name="Intense Reference" w:uiPriority="32" w:qFormat="1"/>
                    <w:lsdException w:name="Book Title" w:uiPriority="33" w:qFormat="1"/>
                    <w:lsdException w:name="Bibliography" w:semiHidden="1" w:uiPriority="37" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="TOC Heading" w:semiHidden="1" w:uiPriority="39" w:unhideWhenUsed="1"
                                    w:qFormat="1"/>
                    <w:lsdException w:name="Plain Table 1" w:uiPriority="41"/>
                    <w:lsdException w:name="Plain Table 2" w:uiPriority="42"/>
                    <w:lsdException w:name="Plain Table 3" w:uiPriority="43"/>
                    <w:lsdException w:name="Plain Table 4" w:uiPriority="44"/>
                    <w:lsdException w:name="Plain Table 5" w:uiPriority="45"/>
                    <w:lsdException w:name="Grid Table Light" w:uiPriority="40"/>
                    <w:lsdException w:name="Grid Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="Grid Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="Grid Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="Grid Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="Grid Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="Grid Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="Grid Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="Grid Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 1" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 1" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 1" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 1" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 1" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 1" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 1" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 2" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 2" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 2" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 2" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 2" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 2" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 2" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 3" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 3" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 3" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 3" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 3" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 3" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 3" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 4" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 4" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 4" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 4" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 4" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 4" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 4" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 5" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 5" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 5" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 5" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 5" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 5" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 5" w:uiPriority="52"/>
                    <w:lsdException w:name="List Table 1 Light Accent 6" w:uiPriority="46"/>
                    <w:lsdException w:name="List Table 2 Accent 6" w:uiPriority="47"/>
                    <w:lsdException w:name="List Table 3 Accent 6" w:uiPriority="48"/>
                    <w:lsdException w:name="List Table 4 Accent 6" w:uiPriority="49"/>
                    <w:lsdException w:name="List Table 5 Dark Accent 6" w:uiPriority="50"/>
                    <w:lsdException w:name="List Table 6 Colorful Accent 6" w:uiPriority="51"/>
                    <w:lsdException w:name="List Table 7 Colorful Accent 6" w:uiPriority="52"/>
                    <w:lsdException w:name="Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Hyperlink" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Hashtag" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Unresolved Mention" w:semiHidden="1" w:unhideWhenUsed="1"/>
                    <w:lsdException w:name="Smart Link" w:semiHidden="1" w:unhideWhenUsed="1"/>
                </w:latentStyles>
                <w:style w:type="paragraph" w:default="1" w:styleId="a">
                    <w:name w:val="Normal"/>
                    <w:qFormat/>
                    <w:rsid w:val="009A0552"/>
                    <w:pPr>
                        <w:widowControl w:val="0"/>
                        <w:spacing w:line="360" w:lineRule="auto"/>
                        <w:jc w:val="both"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="Calibri" w:eastAsia="宋体" w:hAnsi="Calibri" w:cs="Times New Roman"/>
                        <w:sz w:val="24"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="2">
                    <w:name w:val="heading 2"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:link w:val="20"/>
                    <w:qFormat/>
                    <w:rsid w:val="009A0552"/>
                    <w:pPr>
                        <w:outlineLvl w:val="1"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="Cambria" w:hAnsi="Cambria"/>
                        <w:b/>
                        <w:bCs/>
                        <w:kern w:val="0"/>
                        <w:sz w:val="30"/>
                        <w:szCs w:val="32"/>
                        <w:lang w:val="x-none" w:eastAsia="x-none"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:default="1" w:styleId="a0">
                    <w:name w:val="Default Paragraph Font"/>
                    <w:uiPriority w:val="1"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="table" w:default="1" w:styleId="a1">
                    <w:name w:val="Normal Table"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                    <w:tblPr>
                        <w:tblInd w:w="0" w:type="dxa"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPr>
                </w:style>
                <w:style w:type="numbering" w:default="1" w:styleId="a2">
                    <w:name w:val="No List"/>
                    <w:uiPriority w:val="99"/>
                    <w:semiHidden/>
                    <w:unhideWhenUsed/>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a3">
                    <w:name w:val="header"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a4"/>
                    <w:uiPriority w:val="99"/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="009A0552"/>
                    <w:pPr>
                        <w:pBdr>
                            <w:bottom w:val="single" w:sz="6" w:space="1" w:color="auto"/>
                        </w:pBdr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="center"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a4">
                    <w:name w:val="页眉 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="a3"/>
                    <w:uiPriority w:val="99"/>
                    <w:rsid w:val="009A0552"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a5">
                    <w:name w:val="footer"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="a6"/>
                    <w:uiPriority w:val="99"/>
                    <w:unhideWhenUsed/>
                    <w:rsid w:val="009A0552"/>
                    <w:pPr>
                        <w:tabs>
                            <w:tab w:val="center" w:pos="4153"/>
                            <w:tab w:val="right" w:pos="8306"/>
                        </w:tabs>
                        <w:snapToGrid w:val="0"/>
                        <w:jc w:val="left"/>
                    </w:pPr>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="a6">
                    <w:name w:val="页脚 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="a5"/>
                    <w:uiPriority w:val="99"/>
                    <w:rsid w:val="009A0552"/>
                    <w:rPr>
                        <w:sz w:val="18"/>
                        <w:szCs w:val="18"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="20">
                    <w:name w:val="标题 2 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="2"/>
                    <w:rsid w:val="009A0552"/>
                    <w:rPr>
                        <w:rFonts w:ascii="Cambria" w:eastAsia="宋体" w:hAnsi="Cambria" w:cs="Times New Roman"/>
                        <w:b/>
                        <w:bCs/>
                        <w:kern w:val="0"/>
                        <w:sz w:val="30"/>
                        <w:szCs w:val="32"/>
                        <w:lang w:val="x-none" w:eastAsia="x-none"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:styleId="a7">
                    <w:name w:val="caption"/>
                    <w:basedOn w:val="a"/>
                    <w:next w:val="a"/>
                    <w:uiPriority w:val="35"/>
                    <w:unhideWhenUsed/>
                    <w:qFormat/>
                    <w:rsid w:val="009A0552"/>
                    <w:rPr>
                        <w:rFonts w:asciiTheme="majorHAnsi" w:eastAsia="黑体" w:hAnsiTheme="majorHAnsi"
                                  w:cstheme="majorBidi"/>
                        <w:sz w:val="20"/>
                        <w:szCs w:val="20"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="paragraph" w:customStyle="1" w:styleId="w">
                    <w:name w:val="w表格文本居中"/>
                    <w:basedOn w:val="a"/>
                    <w:link w:val="w0"/>
                    <w:qFormat/>
                    <w:rsid w:val="009A0552"/>
                    <w:pPr>
                        <w:spacing w:line="240" w:lineRule="auto"/>
                        <w:jc w:val="center"/>
                    </w:pPr>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                        <w:sz w:val="21"/>
                        <w:szCs w:val="21"/>
                    </w:rPr>
                </w:style>
                <w:style w:type="character" w:customStyle="1" w:styleId="w0">
                    <w:name w:val="w表格文本居中 字符"/>
                    <w:basedOn w:val="a0"/>
                    <w:link w:val="w"/>
                    <w:qFormat/>
                    <w:rsid w:val="009A0552"/>
                    <w:rPr>
                        <w:rFonts w:ascii="宋体" w:eastAsia="宋体" w:hAnsi="宋体" w:cs="宋体"/>
                        <w:szCs w:val="21"/>
                    </w:rPr>
                </w:style>
            </w:styles>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/webSettings.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml">
        <pkg:xmlData>
            <w:webSettings xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                           xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                           xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                           xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                           xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                           xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                           xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                           xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                           xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                           xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                           mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:optimizeForBrowser/>
                <w:allowPNG/>
            </w:webSettings>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/word/fontTable.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml">
        <pkg:xmlData>
            <w:fonts xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
                     xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
                     xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
                     xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
                     xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
                     xmlns:w16cex="http://schemas.microsoft.com/office/word/2018/wordml/cex"
                     xmlns:w16cid="http://schemas.microsoft.com/office/word/2016/wordml/cid"
                     xmlns:w16="http://schemas.microsoft.com/office/word/2018/wordml"
                     xmlns:w16sdtdh="http://schemas.microsoft.com/office/word/2020/wordml/sdtdatahash"
                     xmlns:w16se="http://schemas.microsoft.com/office/word/2015/wordml/symex"
                     mc:Ignorable="w14 w15 w16se w16cid w16 w16cex w16sdtdh">
                <w:font w:name="等线">
                    <w:altName w:val="DengXian"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Times New Roman">
                    <w:panose1 w:val="02020603050405020304"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E0002EFF" w:usb1="C000785B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Calibri">
                    <w:panose1 w:val="020F0502020204030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="swiss"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E4002EFF" w:usb1="C000247B" w:usb2="00000009" w:usb3="00000000" w:csb0="000001FF"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="宋体">
                    <w:altName w:val="SimSun"/>
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="00000003" w:usb1="288F0000" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="Cambria">
                    <w:panose1 w:val="02040503050406030204"/>
                    <w:charset w:val="00"/>
                    <w:family w:val="roman"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="E00006FF" w:usb1="420024FF" w:usb2="02000000" w:usb3="00000000" w:csb0="0000019F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="等线 Light">
                    <w:panose1 w:val="02010600030101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="auto"/>
                    <w:pitch w:val="variable"/>
                    <w:sig w:usb0="A00002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="0004000F"
                           w:csb1="00000000"/>
                </w:font>
                <w:font w:name="黑体">
                    <w:altName w:val="SimHei"/>
                    <w:panose1 w:val="02010609060101010101"/>
                    <w:charset w:val="86"/>
                    <w:family w:val="modern"/>
                    <w:pitch w:val="fixed"/>
                    <w:sig w:usb0="800002BF" w:usb1="38CF7CFA" w:usb2="00000016" w:usb3="00000000" w:csb0="00040001"
                           w:csb1="00000000"/>
                </w:font>
            </w:fonts>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/core.xml" pkg:contentType="application/vnd.openxmlformats-package.core-properties+xml"
              pkg:padding="256">
        <pkg:xmlData>
            <cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties"
                               xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/"
                               xmlns:dcmitype="http://purl.org/dc/dcmitype/"
                               xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <dc:title/>
                <dc:subject/>
                <dc:creator>zhouyabing</dc:creator>
                <cp:keywords/>
                <dc:description/>
                <cp:lastModifiedBy>zhouyabing</cp:lastModifiedBy>
                <cp:revision>2</cp:revision>
                <dcterms:created xsi:type="dcterms:W3CDTF">2021-11-08T03:13:00Z</dcterms:created>
                <dcterms:modified xsi:type="dcterms:W3CDTF">2021-11-08T03:13:00Z</dcterms:modified>
            </cp:coreProperties>
        </pkg:xmlData>
    </pkg:part>
    <pkg:part pkg:name="/docProps/app.xml"
              pkg:contentType="application/vnd.openxmlformats-officedocument.extended-properties+xml" pkg:padding="256">
        <pkg:xmlData>
            <Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties"
                        xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes">
                <Template>Normal.dotm</Template>
                <TotalTime>0</TotalTime>
                <Pages>1</Pages>
                <Words>29</Words>
                <Characters>170</Characters>
                <Application>Microsoft Office Word</Application>
                <DocSecurity>0</DocSecurity>
                <Lines>1</Lines>
                <Paragraphs>1</Paragraphs>
                <ScaleCrop>false</ScaleCrop>
                <Company/>
                <LinksUpToDate>false</LinksUpToDate>
                <CharactersWithSpaces>198</CharactersWithSpaces>
                <SharedDoc>false</SharedDoc>
                <HyperlinksChanged>false</HyperlinksChanged>
                <AppVersion>16.0000</AppVersion>
            </Properties>
        </pkg:xmlData>
    </pkg:part>
</pkg:package>', NULL, 'word.docx.vm'
       , 'word.docx', NULL, '2021-11-30 09:34:35', NULL, '2021-12-02 17:12:46'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (16, 'api-文档示例', '# context   全局上下文，接口传入的json对象
${context}

# content   当前内容， json内容为arry对象时
${content}


# key   json内容为json对象时，每一个属性的key
${key}

# value   json内容为json对象时，每一个属性的value
${value}', NULL, 'jsonDemo.vm'
       , '/#{fileName}.txt', NULL, '2021-12-31 10:56:15', NULL, '2021-12-31 15:45:54'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (17, 'api-实体类', 'package com.yunli.smartcity8.data.midplatform.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


@EqualsAndHashCode(callSuper = true)
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ${key} {
#foreach ($propertie in $value.properties.entrySet())

    @ApiModelProperty("$propertie.value.description")
#if($propertie.value.type == ''string'')
    private String $propertie.key;
#elseif($propertie.value.type == ''integer'')
    private Integer $propertie.key;
#elseif($propertie.value.type == ''boolean'')
    private Boolean $propertie.key;
#elseif($propertie.value.type == ''number'')
    private $propertie.value.format $propertie.key;
#elseif($propertie.value.type == ''object'')
    private $propertie.value.originalRef $propertie.key;
#elseif($propertie.value.type == ''array'')
    private $propertie.value.items.originalRef $propertie.key;
#else
    private $propertie.value.originalRef $propertie.key;
#end
#end
}
', NULL, 'apiEntity.vm'
       , '/#{fileName}.java', NULL, '2021-12-31 14:04:19', NULL, '2021-12-31 15:45:43'
       , NULL, 0);
INSERT INTO `template_management`
VALUES (18, 'feingh接口', 'package com.yunli.smartcity8.data.midplatform.feign;



import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

import java.util.List;

/**
 * @Description: 数据中台接口服务feign
 */
@FeignClient(name = "dataMidPlatformApi", url = "")
public interface ${content.name}Feign {

#foreach ($path in $context.paths)
#foreach ($pathSet in $path.entrySet())
#foreach ($pathSets in $pathSet.entrySet())
#if($pathSets.value.tags[0] == $content.name)

    /**
     * ${$pathSets.value.summary}
     *
     * @param XXX
     * @return XXX
     */
    @${$pathSets.key}Mapping("$pathSet.key")
    $pathSets.responses.200.schema.originalRef login(
#foreach ($parameter in $$pathSets.value.parameters)
#if($parameter.in == ''path'')
    @PathVariable $parameter.type $parameter.name,
#elseif($parameter.in == ''header'')
    @RequestHeader(HeaderConstant.TOKEN) $parameter.type $parameter.name,
#end
#end
    );

#end
#end
#end
#end

}

', NULL, 'feingh.vm'
       , '/#{fileName}Feign.java', NULL, '2021-12-31 16:42:09', NULL, '2021-12-31 17:15:00'
       , NULL, 0);
SET FOREIGN_KEY_CHECKS = 1;
