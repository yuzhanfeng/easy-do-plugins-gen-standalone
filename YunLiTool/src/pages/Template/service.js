// @ts-ignore

/* eslint-disable */
import { request } from 'umi';

/** 获取列表 POST /template/page */
export async function page(params, options) {
  return request('/api/template/page', {
    method: 'POST',
    params: params,
    data: params,
  });
}

/** 获取详情 GET /template/{id} */
export async function getInfo(id, options) {
  return request('/api/template/'+id, {
    method: 'GET',
  });
}


/** 新建 POST /template */

export async function add(data, options) {
  return request('/api/template', {
    data,
    method: 'POST',
    ...(options || {}),
  });
}


/** 更新 PUT /api/template */

export async function update(data, options) {
  return request('/api/template', {
    data,
    method: 'PUT',
    ...(options || {}),
  });
}

/** 删除 DELETE /api/rule */

export async function remove(data, options) {
  return request('/api/template/' + data, {
    method: 'DELETE',
    ...(options || {}),
  });
}
