import request from '@/utils/request'

// 登录方法
export function login(username, password) {
  return request({
    url: '/oauth-server/oauth/token?client_id=management&client_secret=management-secret&grant_type=password&username='+username+'&password='+password,
    method: 'post'
    // ,
    // data: { username, password }
  })
}

// 刷新方法
export function refreshToken(refreshToken) {
  return request({
    url: '/oauth-server/oauth/token?client_id=c1&client_secret=secret&grant_type=refresh_token&refresh_token='+refreshToken,
    method: 'post'
  })
}

// 获取用户详细信息
export function getInfo(token) {
  return request({
    url: '/oauth-server/oauth/check_token?token='+token,
    method: 'post'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/oauth-server/logout',
    method: 'get'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/code',
    method: 'get'
  })
}
