package plus.easydo.gen;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import plus.easydo.starter.plugins.gen.annotation.EnableEasydoGenServer;

import java.io.IOException;

/**
 * @author yuzhanfeng
 */
@EnableEasydoGenServer
@SpringBootApplication
@MapperScan({"com.baomidou.mybatisplus.samples.quickstart.mapper","plus.easydo.**.mapper"})
public class EasyDoGenStandaloneApplication {

    private static Logger logger = LoggerFactory.getLogger(EasyDoGenStandaloneApplication.class);

    private static String windows = "Windows";

    public static void main(String[] args) {
        SpringApplication.run(EasyDoGenStandaloneApplication.class, args);
        System.out.println("" +
                "███████╗ █████╗ ███████╗██╗   ██╗     ██████╗  ██████╗\n" +
                "██╔════╝██╔══██╗██╔════╝╚██╗ ██╔╝     ██╔══██╗██╔═══██╗\n" +
                "█████╗  ███████║███████╗ ╚████╔╝█████╗██║  ██║██║   ██║\n" +
                "██╔══╝  ██╔══██║╚════██║  ╚██╔╝ ╚════╝██║  ██║██║   ██║\n" +
                "███████╗██║  ██║███████║   ██║        ██████╔╝╚██████╔╝\n" +
                "╚══════╝╚═╝  ╚═╝╚══════╝   ╚═╝        ╚═════╝  ╚═════╝\n" +
                "  ┌─┐┌┬┐┌─┐┬─┐┌┬┐  ┌─┐┬ ┬┌─┐┌─┐┌─┐┌─┐┌─┐  ┬\n" +
                "  └─┐ │ ├─┤├┬┘ │   └─┐│ ││  │  ├┤ └─┐└─┐  │\n" +
                "  └─┘ ┴ ┴ ┴┴└─ ┴   └─┘└─┘└─┘└─┘└─┘└─┘└─┘  o");
        try {
            String os = System.getProperty("os.name");
            if(os.contains(windows)){
                Runtime run = Runtime.getRuntime();
                run.exec("cmd.exe /c start http://localhost:8181");
            }
        } catch (IOException e) {
            logger.error("执行CMD命令异常");
        }
        logger.info("访问地址：http://localhost:8181");
    }

}
