// @ts-ignore

/* eslint-disable */
import { request } from 'umi';

/** 获取列表 POST /datasource/page */
export async function page(params, options) {
  return request('/api/datasource/page', {
    method: 'POST',
    params: params,
    data: params,
  });
}

/** 获取详情 GET /datasource/{id} */
export async function getInfo(id, options) {
  return request('/api/datasource/'+id, {
    method: 'GET',
  });
}


/** 新建 POST /datasource */

export async function add(data, options) {
  return request('/api/datasource', {
    data,
    method: 'POST',
    ...(options || {}),
  });
}


/** 更新 PUT /api/rule */

export async function update(data, options) {
  return request('/api/datasource', {
    data,
    method: 'PUT',
    ...(options || {}),
  });
}

/** 删除 DELETE /api/rule */

export async function remove(data, options) {
  return request('/api/datasource/' + data, {
    method: 'DELETE',
    ...(options || {}),
  });
}
