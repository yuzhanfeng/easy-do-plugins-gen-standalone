import React, { forwardRef, useImperativeHandle, useRef, useState } from 'react';
import { message } from 'antd';
import { ModalForm, ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import { update, getInfo, } from '../service';


const UpdateForm = (props, ref) => {

  /** 编辑窗口的弹窗 */
  const [updateModalVisible, handleUpdateModalVisible] = useState(false);
  /** 绑定form表单 */
  const formRef = useRef();
  /** 模板代码默认提示 */
  const templateCode = require('js-base64').Base64.decode('IyMg5qih5p2/5Y+Y6YeP5L+h5oGv6K+05piOIC0tDQojIyAke3RwbENhdGVnb3J5fSDnlJ/miJDnmoTnsbvlnosNCiMjICR7dGFibGVOYW1lfSDmlbDmja7ooajlkI3np7ANCiMjICR7ZnVuY3Rpb25OYW1lfSDnqIvluo/lkI3np7ANCiMjICR7Q2xhc3NOYW1lfSDnsbvlkI0t6aaW5a2X5q+N5aSn5YaZDQojIyAke2NsYXNzTmFtZX0g57G75ZCNLemmluWtl+avjeWwj+WGmQ0KIyMgJHttb2R1bGVOYW1lfSDmqKHlnZflkI3np7ANCiMjICR7QnVzaW5lc3NOYW1lfSDkuJrliqHlkI3np7AgLemmluWtl+avjeWkp+WGmQ0KIyMgJHtidXNpbmVzc05hbWV9IOS4muWKoeWQjeensCAt6aaW5a2X5q+N5bCP5YaZDQojIyAke2Jhc2VQYWNrYWdlfSDljIXliY3nvIAg5aaCIHh4eC54eC54IOeahCB4eHgNCiMjICR7cGFja2FnZU5hbWV9IOWMhei3r+W+hA0KIyMgJHthdXRob3J9IOS9nOiAhQ0KIyMgJHtkYXRldGltZX0g5b2T5YmN5pe26Ze0Leaooeadv+i/kOihjOaXtueahOaXtumXtA0KIyMgJHtwa0NvbHVtbn0g5Li76ZSu5a2X5q61DQojIyAke2ltcG9ydExpc3R9IOmcgOimgeWvvOWFpeWMheeahOWIl+ihqA0KIyMg5aaC5p6c5a2X5q6157G75Z6L5YyF5ZCrIExPQ0FMX0RBVEVfVElNRSDliJnlr7zlhaUgamF2YS50aW1lLkxvY2FsRGF0ZVRpbWUgY29tLmZhc3RlcnhtbC5qYWNrc29uLmFubm90YXRpb24uSnNvbkZvcm1hdA0KIyMg5aaC5p6c5omA5pyJ5YiX5YyF5ZCrQmlnRGVjaW1hbCDliJnlr7zlhaVqYXZhLm1hdGguQmlnRGVjaW1hbA0KIyMg5aaC5p6c55Sf5oiQ55qE57G75Z6L5piv5piv5pWw5o2u57uT5p6E5YiZ5a+85YWlIGphdmEudXRpbC5MaXN0DQojIyAke3Blcm1pc3Npb25QcmVmaXh9IOabtOWFt+aooeWdl+WQjeWPiuS4muWKoeWQjeiHquWKqOeUn+aIkOeahOadg+mZkOWtl+espiAgIG1vZHVsZU5hbWXvvJpidXNpbmVzc05hbWXvvJoNCiMjICR7Y29sdW1uc30g6KGo55qE5omA5pyJ5a2X5q616ZuG5ZCIDQojIyAke2lzUXVlcnl9IOaYr+WQpueUn+aIkOabtOaWsOS7o+eggQ0KIyMgJHtpc0luc2VydH0g5piv5ZCm55Sf5oiQ5o+S5YWl5Luj56CBDQojIyAke2lzVXBkYXRlfSDmmK/lkKbnlJ/miJDmm7TmlrDku6PnoIENCiMjICR7aXNSZW1vdmV9IOaYr+WQpueUn+aIkOWIoOmZpOS7o+eggQ0KIyMgJHtpc1dyYXBwZXJ9IOaYr+WQpueUn+aIkFdyYXBwZXLku6PnoIENCiMjICR7aXNNYW5hZ2VyfSDmmK/lkKbnlJ/miJBtYW5hZ2Vy5bGC57qnDQoNCiMjICR7dGFibGV9IEdlblRhYmxlIOihqOWvueixoeWxnuaAp+ivtOaYjg0KDQojIyAg57yW5Y+3ICB0YWJsZUlkDQojIyAg6KGo5ZCN56ewICB0YWJsZU5hbWUNCiMjICDooajmj4/ov7AgIHRhYmxlQ29tbWVudA0KIyMgIOWFs+iBlOeItuihqOeahOihqOWQjSAgc3ViVGFibGVOYW1lDQojIyAg5pys6KGo5YWz6IGU54i26KGo55qE5aSW6ZSu5ZCNICBzdWJUYWJsZUZrTmFtZQ0KIyMgIOWunuS9k+exu+WQjeensCjpppblrZfmr43lpKflhpkpIGNsYXNzTmFtZQ0KIyMgIOeUn+aIkOeahOexu+Wei++8iGNydWTljZXooajmk43kvZwgdHJlZeagkeihqOaTjeS9nCBzdWLkuLvlrZDooajmk43kvZzvvIkgdHBsQ2F0ZWdvcnkNCiMjICDnlJ/miJDljIXot6/lvoQgIHBhY2thZ2VOYW1lDQojIyAg55Sf5oiQ5qih5Z2X5ZCNIG1vZHVsZU5hbWUNCiMjICDnlJ/miJDkuJrliqHlkI0gIGJ1c2luZXNzTmFtZQ0KIyMgIOeUn+aIkOWKn+iDveWQjSAgZnVuY3Rpb25OYW1lDQojIyAg5L2c6ICFICBmdW5jdGlvbkF1dGhvcg0KIyMgIOeUn+aIkOS7o+eggeaWueW8j++8iDB6aXDljovnvKnljIUgMeiHquWumuS5iei3r+W+hO+8iSAgZ2VuVHlwZQ0KIyMgIOeUn+aIkOi3r+W+hO+8iOS4jeWhq+m7mOiupOmhueebrui3r+W+hO+8iSBnZW5QYXRoDQojIyAg5Li76ZSu5L+h5oGvICDnsbvlnosgR2VuVGFibGVDb2x1bW4gIHBrQ29sdW1uDQojIyAg5a2Q6KGo5L+h5oGvICDnsbvlnosgR2VuVGFibGUgc3ViVGFibGUgDQojIyAg6KGo5YiX5L+h5oGvIExpc3Q8R2VuVGFibGVDb2x1bW4+IGNvbHVtbnMNCiMjICDlhbblroPnlJ/miJDpgInpobkgIG9wdGlvbnMNCiMjICDmoJHnvJbnoIHlrZfmrrUgIHRyZWVDb2RlDQojIyAg5qCR54i257yW56CB5a2X5q61ICB0cmVlUGFyZW50Q29kZQ0KIyMgIOagkeWQjeensOWtl+autSAgdHJlZU5hbWUNCiMjICDkuIrnuqfoj5zljZVJROWtl+autSBwYXJlbnRNZW51SWQNCiMjICDnuqfoj5zljZXlkI3np7DlrZfmrrUgcGFyZW50TWVudU5hbWUNCiMjICDmmK/lkKbnlJ/miJDmn6Xor6IgaXNRdWVyeQ0KIyMgIOaYr+WQpueUn+aIkOaPkuWFpSAgaXNJbnNlcnQNCiMjICDmmK/lkKbnlJ/miJDmm7TmlrAgIGlzVXBkYXRlDQojIyAg5piv5ZCm55Sf5oiQ5Yig6ZmkICBpc1JlbW92ZQ0KIyMgIOaYr+WQpueUn+aIkExhbWJkYeadoeS7tuivreWPpSBpc1dyYXBwZXINCiMjICDmmK/lkKbnlJ/miJBNYW5hZ2Vy5bGCIGlzTWFuYWdlcg0KIyMgIOS9v+eUqOeahOaooeadv2lkICB0ZW1wbGF0ZUlkcw0KDQojIyAke3RhYmxlLnBrQ29sdW1ufSBHZW5UYWJsZUNvbHVtbiDkuLvplK7kv6Hmga/lsZ7mgKfor7TmmI4NCg0KICMjICAgICDnvJblj7cgY29sdW1uSWQNCiAjIyAgICDlvZLlsZ7ooajnvJblj7cgdGFibGVJZA0KICMjICAgIOWIl+WQjeensCAgY29sdW1uTmFtZQ0KICMjICAgIOWIl+aPj+i/sCAgY29sdW1uQ29tbWVudA0KICMjICAgIOWIl+exu+WeiyAgY29sdW1uVHlwZQ0KICMjICAgIEpBVkHnsbvlnosgICBqYXZhVHlwZQ0KICMjICAgIEpBVkHlrZfmrrXlkI0gIGphdmFGaWVsZA0KICMjICAgIOaYr+WQpuS4u+mUru+8iDHmmK/vvIkgaXNQaw0KICMjICAgIOaYr+WQpuW/heWhq++8iDHmmK/vvIkgaXNSZXF1aXJlZA0KICMjICAgIOaYr+WQpuS4uuaPkuWFpeWtl+aute+8iDHmmK/vvIkgIGlzSW5zZXJ0DQogIyMgICAg5piv5ZCm57yW6L6R5a2X5q6177yIMeaYr++8iSAgIGlzRWRpdA0KICMjICAgIOaYr+WQpuWIl+ihqOWtl+aute+8iDHmmK/vvIkgICBpc0xpc3QNCiAjIyAgICDmmK/lkKbmn6Xor6LlrZfmrrXvvIgx5piv77yJICAgaXNRdWVyeQ0KICMjICAgIOafpeivouaWueW8jyBxdWVyeVR5cGXvvIhFUeetieS6juOAgU5F5LiN562J5LqO44CBR1TlpKfkuo7jgIFMVOWwj+S6juOAgUxJS0XmqKHns4rjgIFCRVRXRUVO6IyD5Zu077yJICANCiAjIyAgICDmmL7npLrnsbvlnosgaHRtbFR5cGXvvIhpbnB1dOaWh+acrOahhuOAgXRleHRhcmVh5paH5pys5Z+f44CBc2VsZWN05LiL5ouJ5qGG44CBY2hlY2tib3jlpI3pgInmoYbjgIFyYWRpb+WNlemAieahhuOAgWRhdGV0aW1l5pel5pyf5o6n5Lu244CBaW1hZ2Xlm77niYfkuIrkvKDmjqfku7bjgIF1cGxvYWTmlofku7bkuIrkvKDmjqfku7bjgIFlZGl0b3Llr4zmlofmnKzmjqfku7bvvIkNCiAjIyAgICDlrZflhbjnsbvlnosgZGljdFR5cGUNCiAjIyAgICDmjpLluo8gICBzb3J0');
  /** 生成路径提示信息 */
  const pathDefault = "#{packageName}包路径\#{className}类名\#{businessName}业务名\#{moduleName}模块名";

    /**
   * 获得详情
   *
   * @param currentRow
   */

     const handleGetinfo = async (currentRow) => {
      formRef.current.resetFields();
      const hide = message.loading('加载详情');
      try {
       const result = await getInfo(currentRow.id);
        hide();
        message.success('加载成功');
        formRef.current.setFieldsValue(result.data);
      } catch (error) {
        hide();
        message.error('加载详情失败');
      }
    };


  /**
   * 更新节点
   *
   * @param fields
   */

   const handleUpdate = async (value) => {
    const hide = message.loading('正在更新');

    try {
      value.id=props.currentRow.id;
      await update(value);
      hide();
      message.success('更新成功');
      return true;
    } catch (error) {
      hide();
      message.error('更新失败请重试！');
      return false;
    }
  };

  /** 
   * 将子类组件的方法暴露给父组件
   * react规定必须使用useImperativeHandle方法，来保存并抛出想要传递给父组件的方法或者数据，
   * 第一个参数是ref,第二个参数是函数，返回想要抛出的对象集合
   * 
  */
  useImperativeHandle(ref, () => ({
    handleUpdateModalVisible,
    handleGetinfo: handleGetinfo,
  }));

  return (
    <ModalForm
    formRef={formRef}
    title="编辑模板"
    width="50%"
    visible={updateModalVisible}
    onFinish={async (value) => {
      const success = await handleUpdate(value);

      if (success) {
        handleUpdateModalVisible(false);

        if (props.actionRef.current) {
          props.actionRef.current.reload();
        }
      }
    }}
    onVisibleChange={handleUpdateModalVisible}
  >
    <ProFormText
      width="90%"
      label="模板名称"
      name="templateName"
      rules={[
        {
          required: true,
          message: '请输入模板名称',
        },
      ]}
      placeholder="模板的名称"
    />
    <ProFormTextArea
      width="90%"
      label="模板代码"
      name="templateCode"
      rules={[
        {
          required: true,
          message: '',
        },
      ]}
      initialValue={templateCode}
    />
    <ProFormText
      width="90%"
      label="文件名"
      name="fileName"
      rules={[
        {
          required: true,
          message: '请输入文件名',
        },
      ]}
      placeholder="用于区分文件"
    />
     <ProFormText
      width="90%"
      label="生成路径"
      name="filePath"
      rules={[
        {
          required: true,
          message: '请输入生成路径',
        },
      ]}
      placeholder="文件生成后所在路径"
      initialValue={pathDefault}
    />
    <ProFormTextArea
      width="90%"
      label="备注"
      name="remark"
      placeholder="备注信息"
    />
  </ModalForm>
  );
};

export default forwardRef(UpdateForm);
