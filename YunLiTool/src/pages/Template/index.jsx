import { PlusOutlined } from '@ant-design/icons';
import { Button, message, Drawer } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import ProTable from '@ant-design/pro-table';
import ProDescriptions from '@ant-design/pro-descriptions';
import UpdateForm from './components/UpdateForm';
import CreateForm from './components/CreateForm';
import { page, remove } from './service';



const TableList = () => {





/** 绑定添加页面 */
const createFromRef = useRef();
/** 绑定编辑页面 */
const updateFromRef = useRef();

  const [showDetail, setShowDetail] = useState(false);
  const actionRef = useRef();
  const [currentRow, setCurrentRow] = useState();
  const [selectedRowsState, setSelectedRows] = useState([]);






  /**
   * 删除
   *
   * @param selectedRow
   */

  const handleRemove = async (selectedRow) => {
    console.log(selectedRow)
    const hide = message.loading('正在删除');
    if (!selectedRow) return true;

    try {
      await remove(selectedRow.id);
      hide();
      message.success('删除成功，即将刷新');
      actionRef.current.reloadAndRest();
      return true;
    } catch (error) {
      hide();
      message.error('删除失败，请重试');
      return false;
    }


  };

  /** 表格的刷新对象*/
  /** 国际化配置 */

  const columns = [
    {
      title: '唯一标识',
      dataIndex: 'id',
      tip: '唯一标识',
      render: (dom, entity) => {
        return (
          <a
            onClick={() => {
              setCurrentRow(entity);
              setShowDetail(true);
            }}
          >
            {dom}
          </a>
        );
      },
    },
    {
      title: '模板名称',
      dataIndex: 'templateName',
      valueType: 'text',
    },
    {
      title: '文件名',
      dataIndex: 'fileName',
      valueType: 'text',
    },
    {
      title: '创建时间',
      dataIndex: 'dateTime',
    },
    {
      title: '操作',
      dataIndex: 'option',
      valueType: 'option',
      render: (_, record) => [
        <a
          key="config"
          onClick={() => {
            setCurrentRow(record);
            updateFromRef.current.handleGetinfo(record);
            updateFromRef.current.handleUpdateModalVisible(true);
          }}
        >
          编辑
        </a>,
        <a
          key="remove"
          onClick={() => {
            handleRemove(record);
          }}
        >
          删除
        </a>,
      ],
    },
  ];

  return (
    <PageContainer>
      <ProTable
        headerTitle="模板列表"
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              createFromRef.current.handleCreateModalVisible(true);
            }}
          >
            <PlusOutlined /> 新建
          </Button>,
        ]}
        request={page}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />


      <CreateForm
        ref={createFromRef}
        actionRef={actionRef}
      />

      <UpdateForm
        ref={updateFromRef}
        actionRef={actionRef}
        currentRow={currentRow}
      />

      <Drawer
        width={600}
        visible={showDetail}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {currentRow?.name && (
          <ProDescriptions
            column={2}
            title={currentRow?.name}
            request={async () => ({
              data: currentRow || {},
            })}
            params={{
              id: currentRow?.name,
            }}
            columns={columns}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};

export default TableList;
