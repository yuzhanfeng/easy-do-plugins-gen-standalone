import request from '@/utils/request'

// 查询模板管理列表
export function listTemplate(query) {
  return request({
    url: '/template/page',
    method: 'post',
    data: query
  })
}

// 查询所有模板管理列表
export function templateAll(query) {
  return request({
    url: '/template/list',
    method: 'post',
    data: query
  })
}

// 查询模板管理详细
export function getTemplate(id) {
  return request({
    url: '/template/' + id,
    method: 'get'
  })
}

// 新增模板管理
export function addTemplate(data) {
  return request({
    url: '/template',
    method: 'post',
    data: data
  })
}

// 修改模板管理
export function updateTemplate(data) {
  return request({
    url: '/template',
    method: 'put',
    data: data
  })
}

// 删除模板管理
export function delTemplate(id) {
  return request({
    url: '/template/' + id,
    method: 'delete'
  })
}
